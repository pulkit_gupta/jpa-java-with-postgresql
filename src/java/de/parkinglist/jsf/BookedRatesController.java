package de.parkinglist.jsf;

import de.parkinglist.entities.BookedRates;
import de.parkinglist.jsf.util.JsfUtil;
import de.parkinglist.jsf.util.PaginationHelper;
import de.parkinglist.sessionBeans.BookedRatesFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "bookedRatesController")
@SessionScoped
public class BookedRatesController implements Serializable {

    private BookedRates current;
    private DataModel items = null;
    @EJB
    private de.parkinglist.sessionBeans.BookedRatesFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public BookedRatesController() {
    }

    public BookedRates getSelected() {
        if (current == null) {
            current = new BookedRates();
            current.setBookedRatesPK(new de.parkinglist.entities.BookedRatesPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private BookedRatesFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (BookedRates) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new BookedRates();
        current.setBookedRatesPK(new de.parkinglist.entities.BookedRatesPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getBookedRatesPK().setRateid(current.getLongtermparkingrate().getRateid());
            current.getBookedRatesPK().setBookingid(current.getCarparkbooking().getBookingid());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BookedRatesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (BookedRates) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getBookedRatesPK().setRateid(current.getLongtermparkingrate().getRateid());
            current.getBookedRatesPK().setBookingid(current.getCarparkbooking().getBookingid());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BookedRatesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (BookedRates) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BookedRatesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = BookedRates.class)
    public static class BookedRatesControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            BookedRatesController controller = (BookedRatesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "bookedRatesController");
            return controller.ejbFacade.find(getKey(value));
        }

        de.parkinglist.entities.BookedRatesPK getKey(String value) {
            de.parkinglist.entities.BookedRatesPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new de.parkinglist.entities.BookedRatesPK();
            key.setBookingid(Integer.parseInt(values[0]));
            key.setRateid(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(de.parkinglist.entities.BookedRatesPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getBookingid());
            sb.append(SEPARATOR);
            sb.append(value.getRateid());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof BookedRates) {
                BookedRates o = (BookedRates) object;
                return getStringKey(o.getBookedRatesPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + BookedRates.class.getName());
            }
        }

    }

}
