
package de.parkinglist.controllers;

import de.parkinglist.entities.Airportpricing;
import de.parkinglist.entities.AirportPricelist;
import de.parkinglist.entities.Parkarea;
import de.parkinglist.sessionBeans.AirportPricelistFacade;
import de.parkinglist.sessionBeans.AirportpricingFacade;
import de.parkinglist.sessionBeans.ParkareaFacade;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
        

/**
 *
 * Class containing all the business Logic for the price system of Airport.
 * 
 * It uses the EJB Facades of the airportpricingFacade, airportPricelistFacade, parkAreaFacade
 * 
 * @author pulkit
 */
@Stateless
public class AirportPriceManager {
    protected static final Logger logger = LogManager.getLogger(AirportPriceManager.class.getName());
    
    @EJB
    private de.parkinglist.sessionBeans.AirportpricingFacade airportpricingFacade;
    
    @EJB
    private de.parkinglist.sessionBeans.AirportPricelistFacade airportPricelistFacade;
    
    @EJB
    private de.parkinglist.sessionBeans.ParkareaFacade parkAreaFacade;
    
    
    private AirportpricingFacade getAirportpricingFacade() {
        if(airportpricingFacade==null){
            logger.error("airportpricingFacade can not be generated.");
        }
        return airportpricingFacade;
    }
    
     private AirportPricelistFacade getairportPriceListFacade() {
        if(airportPricelistFacade==null){
            logger.error("airportPricelistFacade can not be generated.");
        }
        return airportPricelistFacade;
    }
     
      private ParkareaFacade getParkAreaFacade() {
        if(parkAreaFacade==null){
            logger.error("airportPricelistFacade can not be generated.");
        }
        return parkAreaFacade;
    }
              
    /**
     * Empty constructor for EJB
     */  
    public AirportPriceManager(){
        
    }
    
    /**
     * Function to calculate the price with from and to date and respective park area.
     * 
     * @param fromDate
     * @param tillDate
     * @param parkAreaId
     * @return 
     */
    public double getPrice(Date fromDate, Date tillDate, int parkAreaId) {

        
        //leaving first element
        int[] monthsDaycounts = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        int monthStart = cal.get(Calendar.MONTH)+1;
        
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(tillDate);
        cal2.add(Calendar.DATE, 1);
        
        int monthEnd = cal2.get(Calendar.MONTH)+1;

        ArrayList monthList = new ArrayList();

        monthList.clear();
        while (cal.getTime().before(cal2.getTime())) {

            int month = cal.get(Calendar.MONTH)+1;
            monthsDaycounts[month] += 1;
            if(monthsDaycounts[month] == 1){
                monthList.add(month);
            }
            cal.add(Calendar.DATE, 1);
            //leave the last date its the plus one.
        }

        
        logger.info("Months range is "+ monthStart +" to "+ monthEnd);
        
        List<Airportpricing> airportPricing = getAirportpricingFacade().getListByParkAreaId(parkAreaId, monthList);
        
        logger.info("length of " + airportPricing.size());


        double price = 0;
        int monthIndex = monthStart-1;
        for(int k = 0; k<monthList.size(); k++){                
            monthIndex++;
            if(monthIndex ==13){
                monthIndex = 1;
            }
            //price = monthsDaycounts            
            logger.info("Inclusive days for the month of " + (monthIndex) + " is " + monthsDaycounts[monthIndex]);

            Airportpricing ap = (Airportpricing) airportPricing.get(k);
 
           List<AirportPricelist> pl = ap.getPricelistList(); 
            double amountRemaining =0;
            AirportPricelist p = null;
//            if(pl.size() > monthsDaycounts[monthIndex]){
//                 p = pl.get(monthsDaycounts[monthIndex]);
//            }
//            else{
//                logger.info("choosing the zero index because number limit accesses");
//                 
//                //will also be forwarded outside loop for the addition.
//                p = pl.get(pl.size()-1);
//                  
//                logger.info("choosing the last largest numbr which is "+ p.getNbrday()+"  which has price of "+p.getPrice());
//                                                
//                
//                int difference = monthsDaycounts[monthIndex] - pl.size()+1;
//                
//                logger.info("Difference for the rate calculation for the zeroth index is "+ difference+" days.");
//                                
//                
//                amountRemaining = difference * pl.get(0).getPrice();
//                
//                
//                logger.info("adding the price which is "+difference+" * "+pl.get(0).getPrice()+" = "+amountRemaining);                                                                         
//                
//            }
//            
            p = pl.get(monthsDaycounts[monthIndex]);
            
            price += (p.getPrice()+amountRemaining);
            logger.info("price for "+monthsDaycounts[monthIndex]+" days is "+(p.getPrice()+amountRemaining));        
        }

        return price;
    }
    
    
    /**
     * Update the price list for the list of months for a respective parkarea
     * 
     * @param monthList
     * @param parkAreaId
     * @param arpList 
     */
    public void updatePricelist(List<Integer> monthList, int parkAreaId, List<AirportPricelist> arpList ){
                
            List<Airportpricing> airportPricings = getAirportpricingFacade().getListByParkAreaId(parkAreaId, monthList);        
            
            
            for (Airportpricing airportPricing : airportPricings) {
                 List <AirportPricelist> arpLists = airportPricing.getPricelistList();
                 int i = 0;

                for (AirportPricelist arp : arpLists) {

                    AirportPricelist newPricelist = arpList.get(i);

                    arp.setPrice(newPricelist.getPrice());

                    getairportPriceListFacade().edit(arp);

                    i++;
                }
            }            
                             
    }
    
    
    
    /**
     * Create the new Price list system for a parkarea ID where months can be null which means that its for whole year
     * 
     * @param months
     * @param parkAreaID
     * @param pricelists 
     */
    public void createPriceList( ArrayList<Integer> months, int parkAreaID, List<AirportPricelist> pricelists){
        
        
        Parkarea parkArea = getParkAreaFacade().find(parkAreaID);
        
        //for all months but also gives flexibilities
        
        if(months == null){
            logger.info("No info for the month is provided, hence using all months for this new price system.");
            months = new ArrayList(12);
            for(int i =1;i<=12;i++ ){
                months.add(i);
            }                        
        }
        
        for (Integer month : months) {        
            
            Airportpricing ap = new Airportpricing();
            ap.setMonth(month);
            ap.setStatus("ON");                   
            ap.setParkareaid(parkArea);           
           
            //will also call flush after persist
            try {
                getAirportpricingFacade().save(ap);
            } catch (Exception e) {
                logger.error(e);
            }
           
            //persist 30 days pricelist            
            for (AirportPricelist pricelist : pricelists) {
                
                
                //copied else only one is saved.
                AirportPricelist priceNew = new AirportPricelist(pricelist);
                
                
                
                priceNew.setPricingid(ap);
                try {
                    
                    getairportPriceListFacade().save(priceNew);
                } catch (Exception e) {
                    logger.error(e);
                }
            }
            
            
        }
     
    }

}
