/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.controllers;

import static de.parkinglist.controllers.AirportPriceManager.logger;
import de.parkinglist.entities.AirportPricelist;
import de.parkinglist.entities.Airportpricing;
import de.parkinglist.entities.Parkarea;
import de.parkinglist.sessionBeans.AirportpricingFacade;
import de.parkinglist.sessionBeans.ParkareaFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * Backing bean for the test.xhtml where the GUI for the editing, creating and showing of the airport pricing is done.
 *  
 * @author pulkit
 */


@ManagedBean(name = "airportPricelistController")
@SessionScoped
public class AirportPricelistController implements Serializable{
    
    @EJB
    private AirportpricingFacade airportpricingFacade;
    
    @EJB
    private ParkareaFacade parkareaFacade;
    
    @EJB
    private AirportPriceManager priceManager;
        
    private List<Integer> selectedSaveMonths;
    
    private int parkareaID = 3071;
    
    private boolean createNewButtonEnable = false;

    public boolean isCreateNewButtonEnable() {
        return createNewButtonEnable;
    }

    public void setCreateNewButtonEnable(boolean createNewButtonEnable) {
        this.createNewButtonEnable = createNewButtonEnable;
    }
    private String message;
    private List<Integer> months, parkAreas;
    private int month =8, parkArea;
    private List<AirportPricelist> airportPriceList;    
    private List saveMonths;

    
    public List<Integer> getSelectedSaveMonths() {
        return selectedSaveMonths;
    }

    public void setSelectedSaveMonths(List<Integer> selectedSaveMonths) {
        this.selectedSaveMonths = selectedSaveMonths;
    }
    
    public List getSaveMonths() {
        return saveMonths;
    }

    public void setSaveMonths(List saveMonths) {
        this.saveMonths = saveMonths;
    }

    
    public List<Integer> getMonths() {
        return months;
    }

    public void setMonths(List<Integer> months) {
        this.months = months;
    }

    public List<Integer> getParkAreas() {
        return parkAreas;
    }

    public void setParkAreas(List<Integer> parkAreas) {
        this.parkAreas = parkAreas;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getParkArea() {
        return parkArea;
    }

    public void setParkArea(int parkArea) {
        this.parkArea = parkArea;
    }
           
    
    public int getParkareaID() {
        return parkareaID;
    }

    public void setParkareaID(int parkareaID) {
        this.parkareaID = parkareaID;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
  
    
    private AirportPriceManager getPriceManager(){
        if(priceManager == null){
            logger.error("price manager EJB is not injected.");
        }
        return priceManager;
    }
    
            
    private ParkareaFacade getParkareaFacade() {
        if (parkareaFacade == null) {
            logger.error("parkareaFacade can not be generated.");
        }
        return parkareaFacade;
    }         
            
   private AirportpricingFacade getAirportpricingFacade() {
        if(airportpricingFacade==null){
            logger.error("facade can not be generated.");
        }
        return airportpricingFacade;
    }
    

    public List<AirportPricelist> getAirportPriceList() {
        return airportPriceList;
    }

    public void setAirportPriceList(List<AirportPricelist> airportPriceList) {
        this.airportPriceList = airportPriceList;
    }
    
    
    
    /**
     * Constructor for some custom type initialization
     */
    public AirportPricelistController(){
        this.months = new ArrayList(12);
        for(int i =1; i<=12; i++){
            this.months.add(i);                 
        }
        createNewButtonEnable = false;
        selectedSaveMonths = new ArrayList();
        parkAreas  = new ArrayList();                
        airportPriceList = new ArrayList(32);
        for (int i = 0; i < 32; i++) {
            AirportPricelist arpList = new AirportPricelist();
            arpList.setNbrday(i);
            arpList.setPrice(12.0);
            
            airportPriceList.add(arpList);
        }
    }
    
    
    /**
     * Function fetch some of the data using respective facades and populate the managed means lists and variables.
     */
    @PostConstruct
    public void init() {
        
        logger.info("fetching called at starting");
        ArrayList<Integer> months = new ArrayList<>();
        months.add(month);
        List<Airportpricing> ArpPricing = getAirportpricingFacade().getListByParkAreaId(parkareaID, months);
        this.message = "Price list fetch for the ParkareaID: " + parkareaID + " and for the months-> "+month;
        
        if(!ArpPricing.isEmpty()){
            if (ArpPricing.get(0).getPricelistList() != null) {
                //this.airportPriceList = ArpPricing.get(0).getPricelistList();
            }
        }
        

        List<Parkarea> parkLists = getParkareaFacade().findAll();
        
        for (Parkarea parkList : parkLists) {
            parkAreas.add(parkList.getParkareaid());
        }
        
        Collections.sort(parkAreas);        

    }


    /**
     * Used by input fields in the test.xhtml and called whenever the month or the parkarea list is changed.
     * It loads all the entries for a desires month and parkarea which are selected.
     */
    public void reload(){
        selectedSaveMonths.clear();
        logger.info("loading for deired month => "+month);
        ArrayList<Integer> month_tocall = new ArrayList<>();
        month_tocall.add(month);
        selectedSaveMonths.add(month);
        List<Airportpricing> ArpPricing = getAirportpricingFacade().getListByParkAreaId(parkareaID, month_tocall);
        this.message = "Price list fetch for the ParkareaID: " + parkareaID + " and for the month -> "+month;       
        
        if(!ArpPricing.isEmpty()){
            createNewButtonEnable = false;
            if (!ArpPricing.get(0).getPricelistList().isEmpty()) {
                this.airportPriceList = ArpPricing.get(0).getPricelistList();
            } else {
                logger.info("Found none");
                this.message = "No Data found for the month = >" + month;
            }
        }
        else{
            createNewButtonEnable = true;
            logger.info("There is no airport pricing system for this id");
            this.message = "There is no airport pricing system for this id";
        }
        
                
    }
    
    

    /**
     * Called when the update button is clicked after selection of the desired month which is to be updated with the values present in the Backing bean list.
     * 
     */
    public void updatePricelist(){
         
        System.out.println("Updating the values in the same parkarea id and month");
        
//        ArrayList<Integer> months = new ArrayList();
//        months.add(8);
//        months.add(9);
        //ArrayList<Integer> months2 = (ArrayList<Integer>) this.selectedSaveMonths;
        priceManager.updatePricelist(selectedSaveMonths, parkareaID, this.airportPriceList);
        
    }
    
    /**
     * Used to by default create all new lists for all months in a year for a Parkareaid which is selected.
     */
    public void createNew(){
        
        createNewButtonEnable = false;
        logger.info("creating new park entry with default values for all months");

        priceManager.createPriceList(null, parkareaID, getAirportPriceList());

    }
    
}
