package de.parkinglist.controllers;


import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.event.SelectEvent;


/**
 * Backing beans for the Airport pricing testing with the calender from and to and price calculation is done.
 * 
 * It works with the index.xhtml in this project.
 * @author pulkit
 */
@ManagedBean(name = "dataController")
@RequestScoped
public class AirportPricingController implements Serializable {
     
    @EJB
    AirportPriceManager priceCalculator;
    public AirportPricingController() {
        
        
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();

        cal.add(Calendar.DATE, 7);
        Date till = cal.getTime();

        this.fromDate = today;

        this.toDate = till;

        this.fromDateMin = today;
        this.toDateMin = today;
    }
   
    private Date fromDate;
    private Date toDate;
    private Date toDateMin;
    private Date fromDateMin;

    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getToDateMin() {
        return toDateMin;
    }

    public void setToDateMin(Date toDateMin) {
        this.toDateMin = toDateMin;
    }

    public Date getFromDateMin() {
        return fromDateMin;
    }

    public void setFromDateMin(Date fromDateMin) {
        this.fromDateMin = fromDateMin;
    }

 

    public void onDateSelect(SelectEvent e) {

        this.toDateMin = this.fromDate;

        if (toDateMin.compareTo(toDate) > 0) {
            toDate = toDateMin;
        }

    }

    /**
     * Called on the submit button and price is calculated now only for the Park area is 3071
     */
    public void submit() {
     
        this.price = priceCalculator.getPrice(fromDate, toDate, 3071);;
    }
               
     
}
