/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "airportpricing")
@NamedQueries({
    @NamedQuery(name = "Airportpricing.findAll", query = "SELECT a FROM Airportpricing a"),
    @NamedQuery(name = "Airportpricing.findByPricingid", query = "SELECT a FROM Airportpricing a WHERE a.pricingid = :pricingid"),
    @NamedQuery(name = "Airportpricing.findByStatus", query = "SELECT a FROM Airportpricing a WHERE a.status = :status"),
    @NamedQuery(name = "Airportpricing.findByMonth", query = "SELECT a FROM Airportpricing a WHERE a.month = :month")})
public class Airportpricing implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pricingid")
    private Integer pricingid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;
    @Column(name = "month")
    private Integer month;
    
    @OneToMany(mappedBy = "pricingid")
    @OrderBy("nbrday")
    private List<AirportPricelist> pricelistList;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Airportpricing() {
    }

    public Airportpricing(Integer pricingid) {
        this.pricingid = pricingid;
    }

    public Airportpricing(Integer pricingid, String status) {
        this.pricingid = pricingid;
        this.status = status;
    }

    public Integer getPricingid() {
        return pricingid;
    }

    public void setPricingid(Integer pricingid) {
        this.pricingid = pricingid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public List<AirportPricelist> getPricelistList() {
        return pricelistList;
    }

    public void setPricelistList(List<AirportPricelist> pricelistList) {
        this.pricelistList = pricelistList;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pricingid != null ? pricingid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Airportpricing)) {
            return false;
        }
        Airportpricing other = (Airportpricing) object;
        if ((this.pricingid == null && other.pricingid != null) || (this.pricingid != null && !this.pricingid.equals(other.pricingid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Airportpricing[ pricingid=" + pricingid + " ]";
    }
   
    
}
