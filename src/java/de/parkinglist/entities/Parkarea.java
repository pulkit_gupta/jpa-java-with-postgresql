/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "parkarea")
@NamedQueries({
    @NamedQuery(name = "Parkarea.findAll", query = "SELECT p FROM Parkarea p"),
    @NamedQuery(name = "Parkarea.findByParkareaid", query = "SELECT p FROM Parkarea p WHERE p.parkareaid = :parkareaid"),
    @NamedQuery(name = "Parkarea.findByLatitude", query = "SELECT p FROM Parkarea p WHERE p.latitude = :latitude"),
    @NamedQuery(name = "Parkarea.findByLongitude", query = "SELECT p FROM Parkarea p WHERE p.longitude = :longitude"),
    @NamedQuery(name = "Parkarea.findByStreet", query = "SELECT p FROM Parkarea p WHERE p.street = :street"),
    @NamedQuery(name = "Parkarea.findByZipcode", query = "SELECT p FROM Parkarea p WHERE p.zipcode = :zipcode"),
    @NamedQuery(name = "Parkarea.findByCity", query = "SELECT p FROM Parkarea p WHERE p.city = :city"),
    @NamedQuery(name = "Parkarea.findByCountry", query = "SELECT p FROM Parkarea p WHERE p.country = :country"),
    @NamedQuery(name = "Parkarea.findByStartDate", query = "SELECT p FROM Parkarea p WHERE p.startDate = :startDate"),
    @NamedQuery(name = "Parkarea.findByEndDate", query = "SELECT p FROM Parkarea p WHERE p.endDate = :endDate"),
    @NamedQuery(name = "Parkarea.findByAreatype", query = "SELECT p FROM Parkarea p WHERE p.areatype = :areatype"),
    @NamedQuery(name = "Parkarea.findByScheduletype", query = "SELECT p FROM Parkarea p WHERE p.scheduletype = :scheduletype"),
    @NamedQuery(name = "Parkarea.findByStatus", query = "SELECT p FROM Parkarea p WHERE p.status = :status"),
    @NamedQuery(name = "Parkarea.findByDescription", query = "SELECT p FROM Parkarea p WHERE p.description = :description"),
    @NamedQuery(name = "Parkarea.findBySecret", query = "SELECT p FROM Parkarea p WHERE p.secret = :secret"),
    @NamedQuery(name = "Parkarea.findByPriceMin", query = "SELECT p FROM Parkarea p WHERE p.priceMin = :priceMin"),
    @NamedQuery(name = "Parkarea.findByNrofspaces", query = "SELECT p FROM Parkarea p WHERE p.nrofspaces = :nrofspaces"),
    @NamedQuery(name = "Parkarea.findByMarkeruri", query = "SELECT p FROM Parkarea p WHERE p.markeruri = :markeruri"),
    @NamedQuery(name = "Parkarea.findByTitle", query = "SELECT p FROM Parkarea p WHERE p.title = :title"),
    @NamedQuery(name = "Parkarea.findByPricemodel", query = "SELECT p FROM Parkarea p WHERE p.pricemodel = :pricemodel"),
    @NamedQuery(name = "Parkarea.findBySeoTitle", query = "SELECT p FROM Parkarea p WHERE p.seoTitle = :seoTitle"),
    @NamedQuery(name = "Parkarea.findByCitySeo", query = "SELECT p FROM Parkarea p WHERE p.citySeo = :citySeo"),
    @NamedQuery(name = "Parkarea.findByOpeningTime", query = "SELECT p FROM Parkarea p WHERE p.openingTime = :openingTime"),
    @NamedQuery(name = "Parkarea.findByHeadroom", query = "SELECT p FROM Parkarea p WHERE p.headroom = :headroom"),
    @NamedQuery(name = "Parkarea.findByOperator", query = "SELECT p FROM Parkarea p WHERE p.operator = :operator"),
    @NamedQuery(name = "Parkarea.findByLongterm", query = "SELECT p FROM Parkarea p WHERE p.longterm = :longterm"),
    @NamedQuery(name = "Parkarea.findByGettingThere", query = "SELECT p FROM Parkarea p WHERE p.gettingThere = :gettingThere"),
    @NamedQuery(name = "Parkarea.findByPriceType", query = "SELECT p FROM Parkarea p WHERE p.priceType = :priceType"),
    @NamedQuery(name = "Parkarea.findByOperatorId", query = "SELECT p FROM Parkarea p WHERE p.operatorId = :operatorId")})
public class Parkarea implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "parkareaid")
    private Integer parkareaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private double longitude;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 127)
    @Column(name = "street")
    private String street;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "zipcode")
    private String zipcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 63)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Size(max = 63)
    @Column(name = "areatype")
    private String areatype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 63)
    @Column(name = "scheduletype")
    private String scheduletype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Size(max = 2147483647)
    @Column(name = "secret")
    private String secret;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_min")
    private Double priceMin;
    @Column(name = "nrofspaces")
    private Integer nrofspaces;
    @Size(max = 255)
    @Column(name = "markeruri")
    private String markeruri;
    @Size(max = 120)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "pricemodel")
    private String pricemodel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 130)
    @Column(name = "seo_title")
    private String seoTitle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 63)
    @Column(name = "city_seo")
    private String citySeo;
    @Size(max = 2147483647)
    @Column(name = "opening_time")
    private String openingTime;
    @Size(max = 31)
    @Column(name = "headroom")
    private String headroom;
    @Size(max = 100)
    @Column(name = "operator")
    private String operator;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "longterm")
    private String longterm;
    @Size(max = 2147483647)
    @Column(name = "getting_there")
    private String gettingThere;
    @Size(max = 7)
    @Column(name = "price_type")
    private String priceType;
    @Column(name = "operator_id")
    private Integer operatorId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Carparkfacility> carparkfacilityList;
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @ManyToOne(optional = false)
    private Parkuser userid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Areafacility> areafacilityList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Carparkpricing> carparkpricingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Fotos> fotosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Carparkbooking> carparkbookingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Airportbooking> airportbookingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Carparkservice> carparkserviceList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Areacontact> areacontactList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Airportpricing> airportpricingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Parkareaday> parkareadayList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Areabooking> areabookingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Allowedvehicle> allowedvehicleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<BookingLockingDays> bookingLockingDaysList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkareaid")
    private List<Longtermparkingrate> longtermparkingrateList;

    public Parkarea() {
    }

    public Parkarea(Integer parkareaid) {
        this.parkareaid = parkareaid;
    }

    public Parkarea(Integer parkareaid, double latitude, double longitude, String street, String zipcode, String city, String country, Date startDate, Date endDate, String scheduletype, String status, String pricemodel, String seoTitle, String citySeo, String longterm) {
        this.parkareaid = parkareaid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.street = street;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
        this.startDate = startDate;
        this.endDate = endDate;
        this.scheduletype = scheduletype;
        this.status = status;
        this.pricemodel = pricemodel;
        this.seoTitle = seoTitle;
        this.citySeo = citySeo;
        this.longterm = longterm;
    }

    public Integer getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Integer parkareaid) {
        this.parkareaid = parkareaid;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getAreatype() {
        return areatype;
    }

    public void setAreatype(String areatype) {
        this.areatype = areatype;
    }

    public String getScheduletype() {
        return scheduletype;
    }

    public void setScheduletype(String scheduletype) {
        this.scheduletype = scheduletype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Double getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Double priceMin) {
        this.priceMin = priceMin;
    }

    public Integer getNrofspaces() {
        return nrofspaces;
    }

    public void setNrofspaces(Integer nrofspaces) {
        this.nrofspaces = nrofspaces;
    }

    public String getMarkeruri() {
        return markeruri;
    }

    public void setMarkeruri(String markeruri) {
        this.markeruri = markeruri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPricemodel() {
        return pricemodel;
    }

    public void setPricemodel(String pricemodel) {
        this.pricemodel = pricemodel;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getCitySeo() {
        return citySeo;
    }

    public void setCitySeo(String citySeo) {
        this.citySeo = citySeo;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getHeadroom() {
        return headroom;
    }

    public void setHeadroom(String headroom) {
        this.headroom = headroom;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getLongterm() {
        return longterm;
    }

    public void setLongterm(String longterm) {
        this.longterm = longterm;
    }

    public String getGettingThere() {
        return gettingThere;
    }

    public void setGettingThere(String gettingThere) {
        this.gettingThere = gettingThere;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public List<Carparkfacility> getCarparkfacilityList() {
        return carparkfacilityList;
    }

    public void setCarparkfacilityList(List<Carparkfacility> carparkfacilityList) {
        this.carparkfacilityList = carparkfacilityList;
    }

    public Parkuser getUserid() {
        return userid;
    }

    public void setUserid(Parkuser userid) {
        this.userid = userid;
    }

    public List<Areafacility> getAreafacilityList() {
        return areafacilityList;
    }

    public void setAreafacilityList(List<Areafacility> areafacilityList) {
        this.areafacilityList = areafacilityList;
    }

    public List<Carparkpricing> getCarparkpricingList() {
        return carparkpricingList;
    }

    public void setCarparkpricingList(List<Carparkpricing> carparkpricingList) {
        this.carparkpricingList = carparkpricingList;
    }

    public List<Fotos> getFotosList() {
        return fotosList;
    }

    public void setFotosList(List<Fotos> fotosList) {
        this.fotosList = fotosList;
    }

    public List<Carparkbooking> getCarparkbookingList() {
        return carparkbookingList;
    }

    public void setCarparkbookingList(List<Carparkbooking> carparkbookingList) {
        this.carparkbookingList = carparkbookingList;
    }

    public List<Airportbooking> getAirportbookingList() {
        return airportbookingList;
    }

    public void setAirportbookingList(List<Airportbooking> airportbookingList) {
        this.airportbookingList = airportbookingList;
    }

    public List<Carparkservice> getCarparkserviceList() {
        return carparkserviceList;
    }

    public void setCarparkserviceList(List<Carparkservice> carparkserviceList) {
        this.carparkserviceList = carparkserviceList;
    }

    public List<Areacontact> getAreacontactList() {
        return areacontactList;
    }

    public void setAreacontactList(List<Areacontact> areacontactList) {
        this.areacontactList = areacontactList;
    }

    public List<Airportpricing> getAirportpricingList() {
        return airportpricingList;
    }

    public void setAirportpricingList(List<Airportpricing> airportpricingList) {
        this.airportpricingList = airportpricingList;
    }

    public List<Parkareaday> getParkareadayList() {
        return parkareadayList;
    }

    public void setParkareadayList(List<Parkareaday> parkareadayList) {
        this.parkareadayList = parkareadayList;
    }

    public List<Areabooking> getAreabookingList() {
        return areabookingList;
    }

    public void setAreabookingList(List<Areabooking> areabookingList) {
        this.areabookingList = areabookingList;
    }

    public List<Allowedvehicle> getAllowedvehicleList() {
        return allowedvehicleList;
    }

    public void setAllowedvehicleList(List<Allowedvehicle> allowedvehicleList) {
        this.allowedvehicleList = allowedvehicleList;
    }

    public List<BookingLockingDays> getBookingLockingDaysList() {
        return bookingLockingDaysList;
    }

    public void setBookingLockingDaysList(List<BookingLockingDays> bookingLockingDaysList) {
        this.bookingLockingDaysList = bookingLockingDaysList;
    }

    public List<Longtermparkingrate> getLongtermparkingrateList() {
        return longtermparkingrateList;
    }

    public void setLongtermparkingrateList(List<Longtermparkingrate> longtermparkingrateList) {
        this.longtermparkingrateList = longtermparkingrateList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parkareaid != null ? parkareaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parkarea)) {
            return false;
        }
        Parkarea other = (Parkarea) object;
        if ((this.parkareaid == null && other.parkareaid != null) || (this.parkareaid != null && !this.parkareaid.equals(other.parkareaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Parkarea[ parkareaid=" + parkareaid + " ]";
    }
    
}
