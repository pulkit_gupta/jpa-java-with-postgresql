/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "crowduser")
@NamedQueries({
    @NamedQuery(name = "Crowduser.findAll", query = "SELECT c FROM Crowduser c"),
    @NamedQuery(name = "Crowduser.findByUserid", query = "SELECT c FROM Crowduser c WHERE c.userid = :userid"),
    @NamedQuery(name = "Crowduser.findByEmail", query = "SELECT c FROM Crowduser c WHERE c.email = :email"),
    @NamedQuery(name = "Crowduser.findByTitle", query = "SELECT c FROM Crowduser c WHERE c.title = :title"),
    @NamedQuery(name = "Crowduser.findByName", query = "SELECT c FROM Crowduser c WHERE c.name = :name"),
    @NamedQuery(name = "Crowduser.findByCreated", query = "SELECT c FROM Crowduser c WHERE c.created = :created"),
    @NamedQuery(name = "Crowduser.findByIpadresse", query = "SELECT c FROM Crowduser c WHERE c.ipadresse = :ipadresse"),
    @NamedQuery(name = "Crowduser.findByDescription", query = "SELECT c FROM Crowduser c WHERE c.description = :description"),
    @NamedQuery(name = "Crowduser.findByUserstatus", query = "SELECT c FROM Crowduser c WHERE c.userstatus = :userstatus")})
public class Crowduser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userid")
    private Integer userid;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "email")
    private String email;
    @Size(max = 31)
    @Column(name = "title")
    private String title;
    @Size(max = 63)
    @Column(name = "name")
    private String name;
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Size(max = 63)
    @Column(name = "ipadresse")
    private String ipadresse;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "userstatus")
    private String userstatus;

    public Crowduser() {
    }

    public Crowduser(Integer userid) {
        this.userid = userid;
    }

    public Crowduser(Integer userid, String email, String userstatus) {
        this.userid = userid;
        this.email = email;
        this.userstatus = userstatus;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getIpadresse() {
        return ipadresse;
    }

    public void setIpadresse(String ipadresse) {
        this.ipadresse = ipadresse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userid != null ? userid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Crowduser)) {
            return false;
        }
        Crowduser other = (Crowduser) object;
        if ((this.userid == null && other.userid != null) || (this.userid != null && !this.userid.equals(other.userid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Crowduser[ userid=" + userid + " ]";
    }
    
}
