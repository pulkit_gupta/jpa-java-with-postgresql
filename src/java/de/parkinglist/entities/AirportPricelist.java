/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "airport_pricelist")

@NamedQueries({
    @NamedQuery(name = "AirportPricelist.findAll", query = "SELECT p FROM AirportPricelist p"),
    @NamedQuery(name = "AirportPricelist.findByPriceid", query = "SELECT p FROM AirportPricelist p WHERE p.priceid = :priceid"),
    @NamedQuery(name = "AirportPricelist.findByNbrday", query = "SELECT p FROM AirportPricelist p WHERE p.nbrday = :nbrday"),
    @NamedQuery(name = "AirportPricelist.findByPrice", query = "SELECT p FROM AirportPricelist p WHERE p.price = :price"),
    @NamedQuery(name = "AirportPricelist.findByType", query = "SELECT p FROM AirportPricelist p WHERE p.type = :type"),
    @NamedQuery(name = "AirportPricelist.findByStatus", query = "SELECT p FROM AirportPricelist p WHERE p.status = :status")})
public class AirportPricelist implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "priceid")
    private Integer priceid;
    @Column(name = "nbrday")
    private Integer nbrday;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Size(max = 7)
    @Column(name = "type")
    private String type;
    @Size(max = 3)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "pricingid", referencedColumnName = "pricingid")
    @ManyToOne
    private Airportpricing pricingid;
    
    @Transient
    private boolean canEdit;

    public boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }
   

    public AirportPricelist() {
        canEdit = false;
    }

    public AirportPricelist(Integer priceid) {
        this.priceid = priceid;
    }

    public Integer getPriceid() {
        return priceid;
    }

    public void setPriceid(Integer priceid) {
        this.priceid = priceid;
    }

    public Integer getNbrday() {
        return nbrday;
    }

    public void setNbrday(Integer nbrday) {
        this.nbrday = nbrday;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Airportpricing getPricingid() {
        return pricingid;
    }

    public void setPricingid(Airportpricing pricingid) {
        this.pricingid = pricingid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (priceid != null ? priceid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AirportPricelist)) {
            return false;
        }
        AirportPricelist other = (AirportPricelist) object;
        if ((this.priceid == null && other.priceid != null) || (this.priceid != null && !this.priceid.equals(other.priceid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Pricelist[ priceid=" + priceid + " ]";
    }
    
    /**
     *  Copy constructor
     * @param dublicate
     */
    public AirportPricelist (AirportPricelist dublicate){
        
        this.canEdit = dublicate.canEdit;
        this.nbrday = dublicate.nbrday;
        this.status = dublicate.status;
        this.price = dublicate.price;
        this.type = dublicate.type;        
    }    
   
}
