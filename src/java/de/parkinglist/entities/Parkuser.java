/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "parkuser")
@NamedQueries({
    @NamedQuery(name = "Parkuser.findAll", query = "SELECT p FROM Parkuser p"),
    @NamedQuery(name = "Parkuser.findByUserid", query = "SELECT p FROM Parkuser p WHERE p.userid = :userid"),
    @NamedQuery(name = "Parkuser.findByEmail", query = "SELECT p FROM Parkuser p WHERE p.email = :email"),
    @NamedQuery(name = "Parkuser.findByTitle", query = "SELECT p FROM Parkuser p WHERE p.title = :title"),
    @NamedQuery(name = "Parkuser.findByLastname", query = "SELECT p FROM Parkuser p WHERE p.lastname = :lastname"),
    @NamedQuery(name = "Parkuser.findByForename", query = "SELECT p FROM Parkuser p WHERE p.forename = :forename"),
    @NamedQuery(name = "Parkuser.findByPasswd", query = "SELECT p FROM Parkuser p WHERE p.passwd = :passwd"),
    @NamedQuery(name = "Parkuser.findByRegistrationdate", query = "SELECT p FROM Parkuser p WHERE p.registrationdate = :registrationdate"),
    @NamedQuery(name = "Parkuser.findByIpadresse", query = "SELECT p FROM Parkuser p WHERE p.ipadresse = :ipadresse"),
    @NamedQuery(name = "Parkuser.findByDescription", query = "SELECT p FROM Parkuser p WHERE p.description = :description"),
    @NamedQuery(name = "Parkuser.findByUserstatus", query = "SELECT p FROM Parkuser p WHERE p.userstatus = :userstatus"),
    @NamedQuery(name = "Parkuser.findByNbrproposal", query = "SELECT p FROM Parkuser p WHERE p.nbrproposal = :nbrproposal"),
    @NamedQuery(name = "Parkuser.findByImpressum", query = "SELECT p FROM Parkuser p WHERE p.impressum = :impressum"),
    @NamedQuery(name = "Parkuser.findByLogo1", query = "SELECT p FROM Parkuser p WHERE p.logo1 = :logo1"),
    @NamedQuery(name = "Parkuser.findByAgb", query = "SELECT p FROM Parkuser p WHERE p.agb = :agb"),
    @NamedQuery(name = "Parkuser.findByOperatorId", query = "SELECT p FROM Parkuser p WHERE p.operatorId = :operatorId")})
public class Parkuser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userid")
    private Integer userid;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 31)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "lastname")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "forename")
    private String forename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "passwd")
    private String passwd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "registrationdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationdate;
    @Size(max = 63)
    @Column(name = "ipadresse")
    private String ipadresse;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "userstatus")
    private String userstatus;
    @Column(name = "nbrproposal")
    private Integer nbrproposal;
    @Size(max = 2147483647)
    @Column(name = "impressum")
    private String impressum;
    @Size(max = 255)
    @Column(name = "logo1")
    private String logo1;
    @Size(max = 2147483647)
    @Column(name = "agb")
    private String agb;
    @Column(name = "operator_id")
    private Integer operatorId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private List<Advertisearea> advertiseareaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private List<Parkarea> parkareaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private List<Userrole> userroleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private List<Userdata> userdataList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private List<Blogentry> blogentryList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private List<Uservehicle> uservehicleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private List<Proposal> proposalList;

    public Parkuser() {
    }

    public Parkuser(Integer userid) {
        this.userid = userid;
    }

    public Parkuser(Integer userid, String email, String lastname, String forename, String passwd, Date registrationdate, String userstatus) {
        this.userid = userid;
        this.email = email;
        this.lastname = lastname;
        this.forename = forename;
        this.passwd = passwd;
        this.registrationdate = registrationdate;
        this.userstatus = userstatus;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Date getRegistrationdate() {
        return registrationdate;
    }

    public void setRegistrationdate(Date registrationdate) {
        this.registrationdate = registrationdate;
    }

    public String getIpadresse() {
        return ipadresse;
    }

    public void setIpadresse(String ipadresse) {
        this.ipadresse = ipadresse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    public Integer getNbrproposal() {
        return nbrproposal;
    }

    public void setNbrproposal(Integer nbrproposal) {
        this.nbrproposal = nbrproposal;
    }

    public String getImpressum() {
        return impressum;
    }

    public void setImpressum(String impressum) {
        this.impressum = impressum;
    }

    public String getLogo1() {
        return logo1;
    }

    public void setLogo1(String logo1) {
        this.logo1 = logo1;
    }

    public String getAgb() {
        return agb;
    }

    public void setAgb(String agb) {
        this.agb = agb;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public List<Advertisearea> getAdvertiseareaList() {
        return advertiseareaList;
    }

    public void setAdvertiseareaList(List<Advertisearea> advertiseareaList) {
        this.advertiseareaList = advertiseareaList;
    }

    public List<Parkarea> getParkareaList() {
        return parkareaList;
    }

    public void setParkareaList(List<Parkarea> parkareaList) {
        this.parkareaList = parkareaList;
    }

    public List<Userrole> getUserroleList() {
        return userroleList;
    }

    public void setUserroleList(List<Userrole> userroleList) {
        this.userroleList = userroleList;
    }

    public List<Userdata> getUserdataList() {
        return userdataList;
    }

    public void setUserdataList(List<Userdata> userdataList) {
        this.userdataList = userdataList;
    }

    public List<Blogentry> getBlogentryList() {
        return blogentryList;
    }

    public void setBlogentryList(List<Blogentry> blogentryList) {
        this.blogentryList = blogentryList;
    }

    public List<Uservehicle> getUservehicleList() {
        return uservehicleList;
    }

    public void setUservehicleList(List<Uservehicle> uservehicleList) {
        this.uservehicleList = uservehicleList;
    }

    public List<Proposal> getProposalList() {
        return proposalList;
    }

    public void setProposalList(List<Proposal> proposalList) {
        this.proposalList = proposalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userid != null ? userid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parkuser)) {
            return false;
        }
        Parkuser other = (Parkuser) object;
        if ((this.userid == null && other.userid != null) || (this.userid != null && !this.userid.equals(other.userid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Parkuser[ userid=" + userid + " ]";
    }
    
}
