/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "district")
@NamedQueries({
    @NamedQuery(name = "District.findAll", query = "SELECT d FROM District d"),
    @NamedQuery(name = "District.findByDistrictid", query = "SELECT d FROM District d WHERE d.districtid = :districtid"),
    @NamedQuery(name = "District.findByName", query = "SELECT d FROM District d WHERE d.name = :name"),
    @NamedQuery(name = "District.findByStreet", query = "SELECT d FROM District d WHERE d.street = :street"),
    @NamedQuery(name = "District.findByZipcode", query = "SELECT d FROM District d WHERE d.zipcode = :zipcode"),
    @NamedQuery(name = "District.findByCity", query = "SELECT d FROM District d WHERE d.city = :city"),
    @NamedQuery(name = "District.findByCountry", query = "SELECT d FROM District d WHERE d.country = :country"),
    @NamedQuery(name = "District.findByLatitude", query = "SELECT d FROM District d WHERE d.latitude = :latitude"),
    @NamedQuery(name = "District.findByLongitude", query = "SELECT d FROM District d WHERE d.longitude = :longitude"),
    @NamedQuery(name = "District.findByDescription", query = "SELECT d FROM District d WHERE d.description = :description"),
    @NamedQuery(name = "District.findBySeoTitle", query = "SELECT d FROM District d WHERE d.seoTitle = :seoTitle"),
    @NamedQuery(name = "District.findBySeoDescript", query = "SELECT d FROM District d WHERE d.seoDescript = :seoDescript"),
    @NamedQuery(name = "District.findByStatus", query = "SELECT d FROM District d WHERE d.status = :status"),
    @NamedQuery(name = "District.findByFoto1", query = "SELECT d FROM District d WHERE d.foto1 = :foto1"),
    @NamedQuery(name = "District.findByFoto1Title", query = "SELECT d FROM District d WHERE d.foto1Title = :foto1Title"),
    @NamedQuery(name = "District.findByUrheber1", query = "SELECT d FROM District d WHERE d.urheber1 = :urheber1"),
    @NamedQuery(name = "District.findByUrheber1Link", query = "SELECT d FROM District d WHERE d.urheber1Link = :urheber1Link"),
    @NamedQuery(name = "District.findByUrheber2", query = "SELECT d FROM District d WHERE d.urheber2 = :urheber2"),
    @NamedQuery(name = "District.findByTitle", query = "SELECT d FROM District d WHERE d.title = :title"),
    @NamedQuery(name = "District.findByAdwords", query = "SELECT d FROM District d WHERE d.adwords = :adwords")})
public class District implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "districtid")
    private Integer districtid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "name")
    private String name;
    @Size(max = 120)
    @Column(name = "street")
    private String street;
    @Size(max = 15)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 63)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "country")
    private String country;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Size(max = 120)
    @Column(name = "seo_title")
    private String seoTitle;
    @Size(max = 2147483647)
    @Column(name = "seo_descript")
    private String seoDescript;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @Size(max = 35)
    @Column(name = "foto1")
    private String foto1;
    @Size(max = 35)
    @Column(name = "foto1_title")
    private String foto1Title;
    @Size(max = 21)
    @Column(name = "urheber1")
    private String urheber1;
    @Size(max = 61)
    @Column(name = "urheber1_link")
    private String urheber1Link;
    @Size(max = 21)
    @Column(name = "urheber2")
    private String urheber2;
    @Size(max = 51)
    @Column(name = "title")
    private String title;
    @Size(max = 2147483647)
    @Column(name = "adwords")
    private String adwords;

    public District() {
    }

    public District(Integer districtid) {
        this.districtid = districtid;
    }

    public District(Integer districtid, String name, String country, String status) {
        this.districtid = districtid;
        this.name = name;
        this.country = country;
        this.status = status;
    }

    public Integer getDistrictid() {
        return districtid;
    }

    public void setDistrictid(Integer districtid) {
        this.districtid = districtid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoDescript() {
        return seoDescript;
    }

    public void setSeoDescript(String seoDescript) {
        this.seoDescript = seoDescript;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto1Title() {
        return foto1Title;
    }

    public void setFoto1Title(String foto1Title) {
        this.foto1Title = foto1Title;
    }

    public String getUrheber1() {
        return urheber1;
    }

    public void setUrheber1(String urheber1) {
        this.urheber1 = urheber1;
    }

    public String getUrheber1Link() {
        return urheber1Link;
    }

    public void setUrheber1Link(String urheber1Link) {
        this.urheber1Link = urheber1Link;
    }

    public String getUrheber2() {
        return urheber2;
    }

    public void setUrheber2(String urheber2) {
        this.urheber2 = urheber2;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAdwords() {
        return adwords;
    }

    public void setAdwords(String adwords) {
        this.adwords = adwords;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (districtid != null ? districtid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof District)) {
            return false;
        }
        District other = (District) object;
        if ((this.districtid == null && other.districtid != null) || (this.districtid != null && !this.districtid.equals(other.districtid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.District[ districtid=" + districtid + " ]";
    }
    
}
