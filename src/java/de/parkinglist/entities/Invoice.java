/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "invoice")
@NamedQueries({
    @NamedQuery(name = "Invoice.findAll", query = "SELECT i FROM Invoice i"),
    @NamedQuery(name = "Invoice.findByInvoiceid", query = "SELECT i FROM Invoice i WHERE i.invoiceid = :invoiceid"),
    @NamedQuery(name = "Invoice.findByUsername", query = "SELECT i FROM Invoice i WHERE i.username = :username"),
    @NamedQuery(name = "Invoice.findByInvoicenumber", query = "SELECT i FROM Invoice i WHERE i.invoicenumber = :invoicenumber"),
    @NamedQuery(name = "Invoice.findByNetPrice", query = "SELECT i FROM Invoice i WHERE i.netPrice = :netPrice"),
    @NamedQuery(name = "Invoice.findByGrossPrice", query = "SELECT i FROM Invoice i WHERE i.grossPrice = :grossPrice"),
    @NamedQuery(name = "Invoice.findByVat", query = "SELECT i FROM Invoice i WHERE i.vat = :vat"),
    @NamedQuery(name = "Invoice.findByInvoiceDate", query = "SELECT i FROM Invoice i WHERE i.invoiceDate = :invoiceDate"),
    @NamedQuery(name = "Invoice.findByStatus", query = "SELECT i FROM Invoice i WHERE i.status = :status"),
    @NamedQuery(name = "Invoice.findByFilename", query = "SELECT i FROM Invoice i WHERE i.filename = :filename")})
public class Invoice implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "invoiceid")
    private Integer invoiceid;
    @Size(max = 31)
    @Column(name = "username")
    private String username;
    @Size(max = 31)
    @Column(name = "invoicenumber")
    private String invoicenumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "net_price")
    private double netPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gross_price")
    private double grossPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vat")
    private double vat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "invoice_date")
    @Temporal(TemporalType.DATE)
    private Date invoiceDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "status")
    private String status;
    @Size(max = 31)
    @Column(name = "filename")
    private String filename;
    @JoinColumn(name = "adsareaid", referencedColumnName = "adsareaid")
    @ManyToOne(optional = false)
    private Advertisearea adsareaid;

    public Invoice() {
    }

    public Invoice(Integer invoiceid) {
        this.invoiceid = invoiceid;
    }

    public Invoice(Integer invoiceid, double netPrice, double grossPrice, double vat, Date invoiceDate, String status) {
        this.invoiceid = invoiceid;
        this.netPrice = netPrice;
        this.grossPrice = grossPrice;
        this.vat = vat;
        this.invoiceDate = invoiceDate;
        this.status = status;
    }

    public Integer getInvoiceid() {
        return invoiceid;
    }

    public void setInvoiceid(Integer invoiceid) {
        this.invoiceid = invoiceid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getInvoicenumber() {
        return invoicenumber;
    }

    public void setInvoicenumber(String invoicenumber) {
        this.invoicenumber = invoicenumber;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public double getGrossPrice() {
        return grossPrice;
    }

    public void setGrossPrice(double grossPrice) {
        this.grossPrice = grossPrice;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Advertisearea getAdsareaid() {
        return adsareaid;
    }

    public void setAdsareaid(Advertisearea adsareaid) {
        this.adsareaid = adsareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invoiceid != null ? invoiceid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invoice)) {
            return false;
        }
        Invoice other = (Invoice) object;
        if ((this.invoiceid == null && other.invoiceid != null) || (this.invoiceid != null && !this.invoiceid.equals(other.invoiceid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Invoice[ invoiceid=" + invoiceid + " ]";
    }
    
}
