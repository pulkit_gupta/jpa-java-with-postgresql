/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkservice1")
@NamedQueries({
    @NamedQuery(name = "Carparkservice1.findAll", query = "SELECT c FROM Carparkservice1 c"),
    @NamedQuery(name = "Carparkservice1.findByCpservice1id", query = "SELECT c FROM Carparkservice1 c WHERE c.cpservice1id = :cpservice1id"),
    @NamedQuery(name = "Carparkservice1.findByTitle", query = "SELECT c FROM Carparkservice1 c WHERE c.title = :title"),
    @NamedQuery(name = "Carparkservice1.findByDescription", query = "SELECT c FROM Carparkservice1 c WHERE c.description = :description"),
    @NamedQuery(name = "Carparkservice1.findByStatus", query = "SELECT c FROM Carparkservice1 c WHERE c.status = :status")})
public class Carparkservice1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cpservice1id")
    private Integer cpservice1id;
    @Size(max = 63)
    @Column(name = "title")
    private String title;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkhausid", referencedColumnName = "parkhausid")
    @ManyToOne(optional = false)
    private Parkhaus parkhausid;

    public Carparkservice1() {
    }

    public Carparkservice1(Integer cpservice1id) {
        this.cpservice1id = cpservice1id;
    }

    public Carparkservice1(Integer cpservice1id, String status) {
        this.cpservice1id = cpservice1id;
        this.status = status;
    }

    public Integer getCpservice1id() {
        return cpservice1id;
    }

    public void setCpservice1id(Integer cpservice1id) {
        this.cpservice1id = cpservice1id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkhaus getParkhausid() {
        return parkhausid;
    }

    public void setParkhausid(Parkhaus parkhausid) {
        this.parkhausid = parkhausid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cpservice1id != null ? cpservice1id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkservice1)) {
            return false;
        }
        Carparkservice1 other = (Carparkservice1) object;
        if ((this.cpservice1id == null && other.cpservice1id != null) || (this.cpservice1id != null && !this.cpservice1id.equals(other.cpservice1id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkservice1[ cpservice1id=" + cpservice1id + " ]";
    }
    
}
