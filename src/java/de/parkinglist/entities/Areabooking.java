/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "areabooking")
@NamedQueries({
    @NamedQuery(name = "Areabooking.findAll", query = "SELECT a FROM Areabooking a"),
    @NamedQuery(name = "Areabooking.findByBookingid", query = "SELECT a FROM Areabooking a WHERE a.bookingid = :bookingid"),
    @NamedQuery(name = "Areabooking.findByUserid", query = "SELECT a FROM Areabooking a WHERE a.userid = :userid"),
    @NamedQuery(name = "Areabooking.findByBookingDate", query = "SELECT a FROM Areabooking a WHERE a.bookingDate = :bookingDate"),
    @NamedQuery(name = "Areabooking.findByFromtime", query = "SELECT a FROM Areabooking a WHERE a.fromtime = :fromtime"),
    @NamedQuery(name = "Areabooking.findByTilltimeExpected", query = "SELECT a FROM Areabooking a WHERE a.tilltimeExpected = :tilltimeExpected"),
    @NamedQuery(name = "Areabooking.findByTilltime", query = "SELECT a FROM Areabooking a WHERE a.tilltime = :tilltime"),
    @NamedQuery(name = "Areabooking.findByFee", query = "SELECT a FROM Areabooking a WHERE a.fee = :fee"),
    @NamedQuery(name = "Areabooking.findByStatus", query = "SELECT a FROM Areabooking a WHERE a.status = :status"),
    @NamedQuery(name = "Areabooking.findByNotice", query = "SELECT a FROM Areabooking a WHERE a.notice = :notice"),
    @NamedQuery(name = "Areabooking.findByOutbookingDate", query = "SELECT a FROM Areabooking a WHERE a.outbookingDate = :outbookingDate")})
public class Areabooking implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bookingid")
    private Integer bookingid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userid")
    private int userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "booking_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bookingDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "fromtime")
    private String fromtime;
    @Size(max = 12)
    @Column(name = "tilltime_expected")
    private String tilltimeExpected;
    @Size(max = 50)
    @Column(name = "tilltime")
    private String tilltime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fee")
    private Double fee;
    @Size(max = 31)
    @Column(name = "status")
    private String status;
    @Size(max = 2147483647)
    @Column(name = "notice")
    private String notice;
    @Column(name = "outbooking_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date outbookingDate;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Areabooking() {
    }

    public Areabooking(Integer bookingid) {
        this.bookingid = bookingid;
    }

    public Areabooking(Integer bookingid, int userid, Date bookingDate, String fromtime) {
        this.bookingid = bookingid;
        this.userid = userid;
        this.bookingDate = bookingDate;
        this.fromtime = fromtime;
    }

    public Integer getBookingid() {
        return bookingid;
    }

    public void setBookingid(Integer bookingid) {
        this.bookingid = bookingid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTilltimeExpected() {
        return tilltimeExpected;
    }

    public void setTilltimeExpected(String tilltimeExpected) {
        this.tilltimeExpected = tilltimeExpected;
    }

    public String getTilltime() {
        return tilltime;
    }

    public void setTilltime(String tilltime) {
        this.tilltime = tilltime;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public Date getOutbookingDate() {
        return outbookingDate;
    }

    public void setOutbookingDate(Date outbookingDate) {
        this.outbookingDate = outbookingDate;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookingid != null ? bookingid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Areabooking)) {
            return false;
        }
        Areabooking other = (Areabooking) object;
        if ((this.bookingid == null && other.bookingid != null) || (this.bookingid != null && !this.bookingid.equals(other.bookingid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Areabooking[ bookingid=" + bookingid + " ]";
    }
    
}
