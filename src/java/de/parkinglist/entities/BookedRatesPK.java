/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author pulkit
 */
@Embeddable
public class BookedRatesPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "bookingid")
    private int bookingid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rateid")
    private int rateid;

    public BookedRatesPK() {
    }

    public BookedRatesPK(int bookingid, int rateid) {
        this.bookingid = bookingid;
        this.rateid = rateid;
    }

    public int getBookingid() {
        return bookingid;
    }

    public void setBookingid(int bookingid) {
        this.bookingid = bookingid;
    }

    public int getRateid() {
        return rateid;
    }

    public void setRateid(int rateid) {
        this.rateid = rateid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) bookingid;
        hash += (int) rateid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookedRatesPK)) {
            return false;
        }
        BookedRatesPK other = (BookedRatesPK) object;
        if (this.bookingid != other.bookingid) {
            return false;
        }
        if (this.rateid != other.rateid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.BookedRatesPK[ bookingid=" + bookingid + ", rateid=" + rateid + " ]";
    }
    
}
