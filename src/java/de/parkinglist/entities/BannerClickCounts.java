/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "banner_click_counts")
@NamedQueries({
    @NamedQuery(name = "BannerClickCounts.findAll", query = "SELECT b FROM BannerClickCounts b"),
    @NamedQuery(name = "BannerClickCounts.findByBannerclickid", query = "SELECT b FROM BannerClickCounts b WHERE b.bannerclickid = :bannerclickid"),
    @NamedQuery(name = "BannerClickCounts.findByName", query = "SELECT b FROM BannerClickCounts b WHERE b.name = :name"),
    @NamedQuery(name = "BannerClickCounts.findByCity", query = "SELECT b FROM BannerClickCounts b WHERE b.city = :city"),
    @NamedQuery(name = "BannerClickCounts.findByCounts", query = "SELECT b FROM BannerClickCounts b WHERE b.counts = :counts"),
    @NamedQuery(name = "BannerClickCounts.findByStatus", query = "SELECT b FROM BannerClickCounts b WHERE b.status = :status")})
public class BannerClickCounts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bannerclickid")
    private Integer bannerclickid;
    @Size(max = 21)
    @Column(name = "name")
    private String name;
    @Size(max = 15)
    @Column(name = "city")
    private String city;
    @Column(name = "counts")
    private Integer counts;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;

    public BannerClickCounts() {
    }

    public BannerClickCounts(Integer bannerclickid) {
        this.bannerclickid = bannerclickid;
    }

    public BannerClickCounts(Integer bannerclickid, String status) {
        this.bannerclickid = bannerclickid;
        this.status = status;
    }

    public Integer getBannerclickid() {
        return bannerclickid;
    }

    public void setBannerclickid(Integer bannerclickid) {
        this.bannerclickid = bannerclickid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCounts() {
        return counts;
    }

    public void setCounts(Integer counts) {
        this.counts = counts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bannerclickid != null ? bannerclickid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BannerClickCounts)) {
            return false;
        }
        BannerClickCounts other = (BannerClickCounts) object;
        if ((this.bannerclickid == null && other.bannerclickid != null) || (this.bannerclickid != null && !this.bannerclickid.equals(other.bannerclickid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.BannerClickCounts[ bannerclickid=" + bannerclickid + " ]";
    }
    
}
