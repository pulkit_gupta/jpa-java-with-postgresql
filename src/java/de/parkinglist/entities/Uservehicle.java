/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "uservehicle")
@NamedQueries({
    @NamedQuery(name = "Uservehicle.findAll", query = "SELECT u FROM Uservehicle u"),
    @NamedQuery(name = "Uservehicle.findByVehicleid", query = "SELECT u FROM Uservehicle u WHERE u.vehicleid = :vehicleid"),
    @NamedQuery(name = "Uservehicle.findByVehicletype", query = "SELECT u FROM Uservehicle u WHERE u.vehicletype = :vehicletype"),
    @NamedQuery(name = "Uservehicle.findByMaker", query = "SELECT u FROM Uservehicle u WHERE u.maker = :maker"),
    @NamedQuery(name = "Uservehicle.findByModel", query = "SELECT u FROM Uservehicle u WHERE u.model = :model"),
    @NamedQuery(name = "Uservehicle.findByLicence", query = "SELECT u FROM Uservehicle u WHERE u.licence = :licence"),
    @NamedQuery(name = "Uservehicle.findByColor", query = "SELECT u FROM Uservehicle u WHERE u.color = :color"),
    @NamedQuery(name = "Uservehicle.findByStatus", query = "SELECT u FROM Uservehicle u WHERE u.status = :status")})
public class Uservehicle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vehicleid")
    private Integer vehicleid;
    @Size(max = 15)
    @Column(name = "vehicletype")
    private String vehicletype;
    @Size(max = 15)
    @Column(name = "maker")
    private String maker;
    @Size(max = 15)
    @Column(name = "model")
    private String model;
    @Size(max = 50)
    @Column(name = "licence")
    private String licence;
    @Size(max = 15)
    @Column(name = "color")
    private String color;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @ManyToOne(optional = false)
    private Parkuser userid;

    public Uservehicle() {
    }

    public Uservehicle(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public Uservehicle(Integer vehicleid, String status) {
        this.vehicleid = vehicleid;
        this.status = status;
    }

    public Integer getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkuser getUserid() {
        return userid;
    }

    public void setUserid(Parkuser userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleid != null ? vehicleid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uservehicle)) {
            return false;
        }
        Uservehicle other = (Uservehicle) object;
        if ((this.vehicleid == null && other.vehicleid != null) || (this.vehicleid != null && !this.vehicleid.equals(other.vehicleid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Uservehicle[ vehicleid=" + vehicleid + " ]";
    }
    
}
