/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "prpslcomment")
@NamedQueries({
    @NamedQuery(name = "Prpslcomment.findAll", query = "SELECT p FROM Prpslcomment p"),
    @NamedQuery(name = "Prpslcomment.findByCommentid", query = "SELECT p FROM Prpslcomment p WHERE p.commentid = :commentid"),
    @NamedQuery(name = "Prpslcomment.findByUserid", query = "SELECT p FROM Prpslcomment p WHERE p.userid = :userid"),
    @NamedQuery(name = "Prpslcomment.findByComment", query = "SELECT p FROM Prpslcomment p WHERE p.comment = :comment"),
    @NamedQuery(name = "Prpslcomment.findByDatum", query = "SELECT p FROM Prpslcomment p WHERE p.datum = :datum"),
    @NamedQuery(name = "Prpslcomment.findByStatus", query = "SELECT p FROM Prpslcomment p WHERE p.status = :status")})
public class Prpslcomment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "commentid")
    private Integer commentid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userid")
    private int userid;
    @Size(max = 2147483647)
    @Column(name = "comment")
    private String comment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datum")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "proposalid", referencedColumnName = "proposalid")
    @ManyToOne(optional = false)
    private Proposal proposalid;

    public Prpslcomment() {
    }

    public Prpslcomment(Integer commentid) {
        this.commentid = commentid;
    }

    public Prpslcomment(Integer commentid, int userid, Date datum, String status) {
        this.commentid = commentid;
        this.userid = userid;
        this.datum = datum;
        this.status = status;
    }

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Proposal getProposalid() {
        return proposalid;
    }

    public void setProposalid(Proposal proposalid) {
        this.proposalid = proposalid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commentid != null ? commentid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prpslcomment)) {
            return false;
        }
        Prpslcomment other = (Prpslcomment) object;
        if ((this.commentid == null && other.commentid != null) || (this.commentid != null && !this.commentid.equals(other.commentid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Prpslcomment[ commentid=" + commentid + " ]";
    }
    
}
