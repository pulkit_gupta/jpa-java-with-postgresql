/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "booked_rates")
@NamedQueries({
    @NamedQuery(name = "BookedRates.findAll", query = "SELECT b FROM BookedRates b"),
    @NamedQuery(name = "BookedRates.findByBookingid", query = "SELECT b FROM BookedRates b WHERE b.bookedRatesPK.bookingid = :bookingid"),
    @NamedQuery(name = "BookedRates.findByRateid", query = "SELECT b FROM BookedRates b WHERE b.bookedRatesPK.rateid = :rateid"),
    @NamedQuery(name = "BookedRates.findByAmount", query = "SELECT b FROM BookedRates b WHERE b.amount = :amount"),
    @NamedQuery(name = "BookedRates.findByPrice", query = "SELECT b FROM BookedRates b WHERE b.price = :price"),
    @NamedQuery(name = "BookedRates.findByStartDate", query = "SELECT b FROM BookedRates b WHERE b.startDate = :startDate")})
public class BookedRates implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BookedRatesPK bookedRatesPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private double price;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @JoinColumn(name = "rateid", referencedColumnName = "rateid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Longtermparkingrate longtermparkingrate;
    @JoinColumn(name = "bookingid", referencedColumnName = "bookingid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Carparkbooking carparkbooking;

    public BookedRates() {
    }

    public BookedRates(BookedRatesPK bookedRatesPK) {
        this.bookedRatesPK = bookedRatesPK;
    }

    public BookedRates(BookedRatesPK bookedRatesPK, int amount, double price, Date startDate) {
        this.bookedRatesPK = bookedRatesPK;
        this.amount = amount;
        this.price = price;
        this.startDate = startDate;
    }

    public BookedRates(int bookingid, int rateid) {
        this.bookedRatesPK = new BookedRatesPK(bookingid, rateid);
    }

    public BookedRatesPK getBookedRatesPK() {
        return bookedRatesPK;
    }

    public void setBookedRatesPK(BookedRatesPK bookedRatesPK) {
        this.bookedRatesPK = bookedRatesPK;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Longtermparkingrate getLongtermparkingrate() {
        return longtermparkingrate;
    }

    public void setLongtermparkingrate(Longtermparkingrate longtermparkingrate) {
        this.longtermparkingrate = longtermparkingrate;
    }

    public Carparkbooking getCarparkbooking() {
        return carparkbooking;
    }

    public void setCarparkbooking(Carparkbooking carparkbooking) {
        this.carparkbooking = carparkbooking;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookedRatesPK != null ? bookedRatesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookedRates)) {
            return false;
        }
        BookedRates other = (BookedRates) object;
        if ((this.bookedRatesPK == null && other.bookedRatesPK != null) || (this.bookedRatesPK != null && !this.bookedRatesPK.equals(other.bookedRatesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.BookedRates[ bookedRatesPK=" + bookedRatesPK + " ]";
    }
    
}
