/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "parkareaday1")
@NamedQueries({
    @NamedQuery(name = "Parkareaday1.findAll", query = "SELECT p FROM Parkareaday1 p"),
    @NamedQuery(name = "Parkareaday1.findByPad1id", query = "SELECT p FROM Parkareaday1 p WHERE p.pad1id = :pad1id"),
    @NamedQuery(name = "Parkareaday1.findByAreaday", query = "SELECT p FROM Parkareaday1 p WHERE p.areaday = :areaday"),
    @NamedQuery(name = "Parkareaday1.findByFromtime", query = "SELECT p FROM Parkareaday1 p WHERE p.fromtime = :fromtime"),
    @NamedQuery(name = "Parkareaday1.findByTilltime", query = "SELECT p FROM Parkareaday1 p WHERE p.tilltime = :tilltime"),
    @NamedQuery(name = "Parkareaday1.findByDayofweek", query = "SELECT p FROM Parkareaday1 p WHERE p.dayofweek = :dayofweek"),
    @NamedQuery(name = "Parkareaday1.findByStatus", query = "SELECT p FROM Parkareaday1 p WHERE p.status = :status"),
    @NamedQuery(name = "Parkareaday1.findByNotice", query = "SELECT p FROM Parkareaday1 p WHERE p.notice = :notice"),
    @NamedQuery(name = "Parkareaday1.findByAllday", query = "SELECT p FROM Parkareaday1 p WHERE p.allday = :allday"),
    @NamedQuery(name = "Parkareaday1.findByTilltime2", query = "SELECT p FROM Parkareaday1 p WHERE p.tilltime2 = :tilltime2"),
    @NamedQuery(name = "Parkareaday1.findByFromtime2", query = "SELECT p FROM Parkareaday1 p WHERE p.fromtime2 = :fromtime2"),
    @NamedQuery(name = "Parkareaday1.findByFromtime3", query = "SELECT p FROM Parkareaday1 p WHERE p.fromtime3 = :fromtime3"),
    @NamedQuery(name = "Parkareaday1.findByTilltime3", query = "SELECT p FROM Parkareaday1 p WHERE p.tilltime3 = :tilltime3"),
    @NamedQuery(name = "Parkareaday1.findByPriceHour1", query = "SELECT p FROM Parkareaday1 p WHERE p.priceHour1 = :priceHour1"),
    @NamedQuery(name = "Parkareaday1.findByPriceHour2", query = "SELECT p FROM Parkareaday1 p WHERE p.priceHour2 = :priceHour2"),
    @NamedQuery(name = "Parkareaday1.findByPriceHour3", query = "SELECT p FROM Parkareaday1 p WHERE p.priceHour3 = :priceHour3")})
public class Parkareaday1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pad1id")
    private Integer pad1id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "areaday")
    private String areaday;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "fromtime")
    private String fromtime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "tilltime")
    private String tilltime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dayofweek")
    private int dayofweek;
    @Size(max = 31)
    @Column(name = "status")
    private String status;
    @Size(max = 2147483647)
    @Column(name = "notice")
    private String notice;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "allday")
    private String allday;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "tilltime2")
    private String tilltime2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "fromtime2")
    private String fromtime2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "fromtime3")
    private String fromtime3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "tilltime3")
    private String tilltime3;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_hour1")
    private Double priceHour1;
    @Column(name = "price_hour2")
    private Double priceHour2;
    @Column(name = "price_hour3")
    private Double priceHour3;
    @JoinColumn(name = "parkhausid", referencedColumnName = "parkhausid")
    @ManyToOne(optional = false)
    private Parkhaus parkhausid;

    public Parkareaday1() {
    }

    public Parkareaday1(Integer pad1id) {
        this.pad1id = pad1id;
    }

    public Parkareaday1(Integer pad1id, String areaday, String fromtime, String tilltime, int dayofweek, String allday, String tilltime2, String fromtime2, String fromtime3, String tilltime3) {
        this.pad1id = pad1id;
        this.areaday = areaday;
        this.fromtime = fromtime;
        this.tilltime = tilltime;
        this.dayofweek = dayofweek;
        this.allday = allday;
        this.tilltime2 = tilltime2;
        this.fromtime2 = fromtime2;
        this.fromtime3 = fromtime3;
        this.tilltime3 = tilltime3;
    }

    public Integer getPad1id() {
        return pad1id;
    }

    public void setPad1id(Integer pad1id) {
        this.pad1id = pad1id;
    }

    public String getAreaday() {
        return areaday;
    }

    public void setAreaday(String areaday) {
        this.areaday = areaday;
    }

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTilltime() {
        return tilltime;
    }

    public void setTilltime(String tilltime) {
        this.tilltime = tilltime;
    }

    public int getDayofweek() {
        return dayofweek;
    }

    public void setDayofweek(int dayofweek) {
        this.dayofweek = dayofweek;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getAllday() {
        return allday;
    }

    public void setAllday(String allday) {
        this.allday = allday;
    }

    public String getTilltime2() {
        return tilltime2;
    }

    public void setTilltime2(String tilltime2) {
        this.tilltime2 = tilltime2;
    }

    public String getFromtime2() {
        return fromtime2;
    }

    public void setFromtime2(String fromtime2) {
        this.fromtime2 = fromtime2;
    }

    public String getFromtime3() {
        return fromtime3;
    }

    public void setFromtime3(String fromtime3) {
        this.fromtime3 = fromtime3;
    }

    public String getTilltime3() {
        return tilltime3;
    }

    public void setTilltime3(String tilltime3) {
        this.tilltime3 = tilltime3;
    }

    public Double getPriceHour1() {
        return priceHour1;
    }

    public void setPriceHour1(Double priceHour1) {
        this.priceHour1 = priceHour1;
    }

    public Double getPriceHour2() {
        return priceHour2;
    }

    public void setPriceHour2(Double priceHour2) {
        this.priceHour2 = priceHour2;
    }

    public Double getPriceHour3() {
        return priceHour3;
    }

    public void setPriceHour3(Double priceHour3) {
        this.priceHour3 = priceHour3;
    }

    public Parkhaus getParkhausid() {
        return parkhausid;
    }

    public void setParkhausid(Parkhaus parkhausid) {
        this.parkhausid = parkhausid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pad1id != null ? pad1id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parkareaday1)) {
            return false;
        }
        Parkareaday1 other = (Parkareaday1) object;
        if ((this.pad1id == null && other.pad1id != null) || (this.pad1id != null && !this.pad1id.equals(other.pad1id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Parkareaday1[ pad1id=" + pad1id + " ]";
    }
    
}
