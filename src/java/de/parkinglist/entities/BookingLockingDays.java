/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "booking_locking_days")
@NamedQueries({
    @NamedQuery(name = "BookingLockingDays.findAll", query = "SELECT b FROM BookingLockingDays b"),
    @NamedQuery(name = "BookingLockingDays.findByLockingdaysid", query = "SELECT b FROM BookingLockingDays b WHERE b.lockingdaysid = :lockingdaysid"),
    @NamedQuery(name = "BookingLockingDays.findByFromDate", query = "SELECT b FROM BookingLockingDays b WHERE b.fromDate = :fromDate"),
    @NamedQuery(name = "BookingLockingDays.findByTillDate", query = "SELECT b FROM BookingLockingDays b WHERE b.tillDate = :tillDate"),
    @NamedQuery(name = "BookingLockingDays.findByStatus", query = "SELECT b FROM BookingLockingDays b WHERE b.status = :status")})
public class BookingLockingDays implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "lockingdaysid")
    private Integer lockingdaysid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "from_date")
    @Temporal(TemporalType.DATE)
    private Date fromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "till_date")
    @Temporal(TemporalType.DATE)
    private Date tillDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public BookingLockingDays() {
    }

    public BookingLockingDays(Integer lockingdaysid) {
        this.lockingdaysid = lockingdaysid;
    }

    public BookingLockingDays(Integer lockingdaysid, Date fromDate, Date tillDate, String status) {
        this.lockingdaysid = lockingdaysid;
        this.fromDate = fromDate;
        this.tillDate = tillDate;
        this.status = status;
    }

    public Integer getLockingdaysid() {
        return lockingdaysid;
    }

    public void setLockingdaysid(Integer lockingdaysid) {
        this.lockingdaysid = lockingdaysid;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getTillDate() {
        return tillDate;
    }

    public void setTillDate(Date tillDate) {
        this.tillDate = tillDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lockingdaysid != null ? lockingdaysid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingLockingDays)) {
            return false;
        }
        BookingLockingDays other = (BookingLockingDays) object;
        if ((this.lockingdaysid == null && other.lockingdaysid != null) || (this.lockingdaysid != null && !this.lockingdaysid.equals(other.lockingdaysid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.BookingLockingDays[ lockingdaysid=" + lockingdaysid + " ]";
    }
    
}
