/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "uservote")
@NamedQueries({
    @NamedQuery(name = "Uservote.findAll", query = "SELECT u FROM Uservote u"),
    @NamedQuery(name = "Uservote.findByVoteid", query = "SELECT u FROM Uservote u WHERE u.voteid = :voteid"),
    @NamedQuery(name = "Uservote.findByUseremail", query = "SELECT u FROM Uservote u WHERE u.useremail = :useremail"),
    @NamedQuery(name = "Uservote.findByDatum", query = "SELECT u FROM Uservote u WHERE u.datum = :datum")})
public class Uservote implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "voteid")
    private Integer voteid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "useremail")
    private String useremail;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datum")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;
    @JoinColumn(name = "proposalid", referencedColumnName = "proposalid")
    @ManyToOne(optional = false)
    private Proposal proposalid;

    public Uservote() {
    }

    public Uservote(Integer voteid) {
        this.voteid = voteid;
    }

    public Uservote(Integer voteid, String useremail, Date datum) {
        this.voteid = voteid;
        this.useremail = useremail;
        this.datum = datum;
    }

    public Integer getVoteid() {
        return voteid;
    }

    public void setVoteid(Integer voteid) {
        this.voteid = voteid;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Proposal getProposalid() {
        return proposalid;
    }

    public void setProposalid(Proposal proposalid) {
        this.proposalid = proposalid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (voteid != null ? voteid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uservote)) {
            return false;
        }
        Uservote other = (Uservote) object;
        if ((this.voteid == null && other.voteid != null) || (this.voteid != null && !this.voteid.equals(other.voteid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Uservote[ voteid=" + voteid + " ]";
    }
    
}
