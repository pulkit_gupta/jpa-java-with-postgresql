/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "alarmcontact")
@NamedQueries({
    @NamedQuery(name = "Alarmcontact.findAll", query = "SELECT a FROM Alarmcontact a"),
    @NamedQuery(name = "Alarmcontact.findByAlarmid", query = "SELECT a FROM Alarmcontact a WHERE a.alarmid = :alarmid"),
    @NamedQuery(name = "Alarmcontact.findByEmail", query = "SELECT a FROM Alarmcontact a WHERE a.email = :email"),
    @NamedQuery(name = "Alarmcontact.findByAddress", query = "SELECT a FROM Alarmcontact a WHERE a.address = :address"),
    @NamedQuery(name = "Alarmcontact.findByRadius", query = "SELECT a FROM Alarmcontact a WHERE a.radius = :radius"),
    @NamedQuery(name = "Alarmcontact.findByStatus", query = "SELECT a FROM Alarmcontact a WHERE a.status = :status"),
    @NamedQuery(name = "Alarmcontact.findByIpadresse", query = "SELECT a FROM Alarmcontact a WHERE a.ipadresse = :ipadresse"),
    @NamedQuery(name = "Alarmcontact.findByLongitude", query = "SELECT a FROM Alarmcontact a WHERE a.longitude = :longitude"),
    @NamedQuery(name = "Alarmcontact.findByLatitude", query = "SELECT a FROM Alarmcontact a WHERE a.latitude = :latitude"),
    @NamedQuery(name = "Alarmcontact.findByCity", query = "SELECT a FROM Alarmcontact a WHERE a.city = :city"),
    @NamedQuery(name = "Alarmcontact.findByDatum", query = "SELECT a FROM Alarmcontact a WHERE a.datum = :datum")})
public class Alarmcontact implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "alarmid")
    private Integer alarmid;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 127)
    @Column(name = "email")
    private String email;
    @Size(max = 127)
    @Column(name = "address")
    private String address;
    @Column(name = "radius")
    private Integer radius;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "status")
    private String status;
    @Size(max = 63)
    @Column(name = "ipadresse")
    private String ipadresse;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "longitude")
    private Double longitude;
    @Column(name = "latitude")
    private Double latitude;
    @Size(max = 63)
    @Column(name = "city")
    private String city;
    @Size(max = 40)
    @Column(name = "datum")
    private String datum;

    public Alarmcontact() {
    }

    public Alarmcontact(Integer alarmid) {
        this.alarmid = alarmid;
    }

    public Alarmcontact(Integer alarmid, String status) {
        this.alarmid = alarmid;
        this.status = status;
    }

    public Integer getAlarmid() {
        return alarmid;
    }

    public void setAlarmid(Integer alarmid) {
        this.alarmid = alarmid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIpadresse() {
        return ipadresse;
    }

    public void setIpadresse(String ipadresse) {
        this.ipadresse = ipadresse;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alarmid != null ? alarmid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alarmcontact)) {
            return false;
        }
        Alarmcontact other = (Alarmcontact) object;
        if ((this.alarmid == null && other.alarmid != null) || (this.alarmid != null && !this.alarmid.equals(other.alarmid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Alarmcontact[ alarmid=" + alarmid + " ]";
    }
    
}
