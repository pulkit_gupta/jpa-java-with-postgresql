/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkpricing1")
@NamedQueries({
    @NamedQuery(name = "Carparkpricing1.findAll", query = "SELECT c FROM Carparkpricing1 c"),
    @NamedQuery(name = "Carparkpricing1.findByCppricing1id", query = "SELECT c FROM Carparkpricing1 c WHERE c.cppricing1id = :cppricing1id"),
    @NamedQuery(name = "Carparkpricing1.findByPricing", query = "SELECT c FROM Carparkpricing1 c WHERE c.pricing = :pricing"),
    @NamedQuery(name = "Carparkpricing1.findByStatus", query = "SELECT c FROM Carparkpricing1 c WHERE c.status = :status")})
public class Carparkpricing1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cppricing1id")
    private Integer cppricing1id;
    @Size(max = 2147483647)
    @Column(name = "pricing")
    private String pricing;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkhausid", referencedColumnName = "parkhausid")
    @ManyToOne(optional = false)
    private Parkhaus parkhausid;

    public Carparkpricing1() {
    }

    public Carparkpricing1(Integer cppricing1id) {
        this.cppricing1id = cppricing1id;
    }

    public Carparkpricing1(Integer cppricing1id, String status) {
        this.cppricing1id = cppricing1id;
        this.status = status;
    }

    public Integer getCppricing1id() {
        return cppricing1id;
    }

    public void setCppricing1id(Integer cppricing1id) {
        this.cppricing1id = cppricing1id;
    }

    public String getPricing() {
        return pricing;
    }

    public void setPricing(String pricing) {
        this.pricing = pricing;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkhaus getParkhausid() {
        return parkhausid;
    }

    public void setParkhausid(Parkhaus parkhausid) {
        this.parkhausid = parkhausid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cppricing1id != null ? cppricing1id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkpricing1)) {
            return false;
        }
        Carparkpricing1 other = (Carparkpricing1) object;
        if ((this.cppricing1id == null && other.cppricing1id != null) || (this.cppricing1id != null && !this.cppricing1id.equals(other.cppricing1id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkpricing1[ cppricing1id=" + cppricing1id + " ]";
    }
    
}
