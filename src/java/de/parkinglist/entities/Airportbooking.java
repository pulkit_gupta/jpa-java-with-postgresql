/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "airportbooking")
@NamedQueries({
    @NamedQuery(name = "Airportbooking.findAll", query = "SELECT a FROM Airportbooking a"),
    @NamedQuery(name = "Airportbooking.findByBookingid", query = "SELECT a FROM Airportbooking a WHERE a.bookingid = :bookingid"),
    @NamedQuery(name = "Airportbooking.findByBookingDate", query = "SELECT a FROM Airportbooking a WHERE a.bookingDate = :bookingDate"),
    @NamedQuery(name = "Airportbooking.findByNrofspaces", query = "SELECT a FROM Airportbooking a WHERE a.nrofspaces = :nrofspaces"),
    @NamedQuery(name = "Airportbooking.findByPrice", query = "SELECT a FROM Airportbooking a WHERE a.price = :price"),
    @NamedQuery(name = "Airportbooking.findByAnrede", query = "SELECT a FROM Airportbooking a WHERE a.anrede = :anrede"),
    @NamedQuery(name = "Airportbooking.findByLastname", query = "SELECT a FROM Airportbooking a WHERE a.lastname = :lastname"),
    @NamedQuery(name = "Airportbooking.findByForename", query = "SELECT a FROM Airportbooking a WHERE a.forename = :forename"),
    @NamedQuery(name = "Airportbooking.findByPhone", query = "SELECT a FROM Airportbooking a WHERE a.phone = :phone"),
    @NamedQuery(name = "Airportbooking.findByEmail", query = "SELECT a FROM Airportbooking a WHERE a.email = :email"),
    @NamedQuery(name = "Airportbooking.findByStreet", query = "SELECT a FROM Airportbooking a WHERE a.street = :street"),
    @NamedQuery(name = "Airportbooking.findByZipcode", query = "SELECT a FROM Airportbooking a WHERE a.zipcode = :zipcode"),
    @NamedQuery(name = "Airportbooking.findByCity", query = "SELECT a FROM Airportbooking a WHERE a.city = :city"),
    @NamedQuery(name = "Airportbooking.findByCountry", query = "SELECT a FROM Airportbooking a WHERE a.country = :country"),
    @NamedQuery(name = "Airportbooking.findByCompany", query = "SELECT a FROM Airportbooking a WHERE a.company = :company"),
    @NamedQuery(name = "Airportbooking.findByBankaccountholder", query = "SELECT a FROM Airportbooking a WHERE a.bankaccountholder = :bankaccountholder"),
    @NamedQuery(name = "Airportbooking.findByBank", query = "SELECT a FROM Airportbooking a WHERE a.bank = :bank"),
    @NamedQuery(name = "Airportbooking.findByBankaccount", query = "SELECT a FROM Airportbooking a WHERE a.bankaccount = :bankaccount"),
    @NamedQuery(name = "Airportbooking.findByBankcode", query = "SELECT a FROM Airportbooking a WHERE a.bankcode = :bankcode"),
    @NamedQuery(name = "Airportbooking.findByIban", query = "SELECT a FROM Airportbooking a WHERE a.iban = :iban"),
    @NamedQuery(name = "Airportbooking.findByBic", query = "SELECT a FROM Airportbooking a WHERE a.bic = :bic"),
    @NamedQuery(name = "Airportbooking.findByUstIdnr", query = "SELECT a FROM Airportbooking a WHERE a.ustIdnr = :ustIdnr"),
    @NamedQuery(name = "Airportbooking.findByVehicleLicence", query = "SELECT a FROM Airportbooking a WHERE a.vehicleLicence = :vehicleLicence"),
    @NamedQuery(name = "Airportbooking.findByPersonNbr", query = "SELECT a FROM Airportbooking a WHERE a.personNbr = :personNbr"),
    @NamedQuery(name = "Airportbooking.findByDeparture", query = "SELECT a FROM Airportbooking a WHERE a.departure = :departure"),
    @NamedQuery(name = "Airportbooking.findByDepartureHour", query = "SELECT a FROM Airportbooking a WHERE a.departureHour = :departureHour"),
    @NamedQuery(name = "Airportbooking.findByDepartureFlightNbr", query = "SELECT a FROM Airportbooking a WHERE a.departureFlightNbr = :departureFlightNbr"),
    @NamedQuery(name = "Airportbooking.findByArrival", query = "SELECT a FROM Airportbooking a WHERE a.arrival = :arrival"),
    @NamedQuery(name = "Airportbooking.findByArrivalHour", query = "SELECT a FROM Airportbooking a WHERE a.arrivalHour = :arrivalHour"),
    @NamedQuery(name = "Airportbooking.findByArrivalFlightNbr", query = "SELECT a FROM Airportbooking a WHERE a.arrivalFlightNbr = :arrivalFlightNbr"),
    @NamedQuery(name = "Airportbooking.findByContractNbr", query = "SELECT a FROM Airportbooking a WHERE a.contractNbr = :contractNbr"),
    @NamedQuery(name = "Airportbooking.findByContractTrack", query = "SELECT a FROM Airportbooking a WHERE a.contractTrack = :contractTrack"),
    @NamedQuery(name = "Airportbooking.findByNote", query = "SELECT a FROM Airportbooking a WHERE a.note = :note"),
    @NamedQuery(name = "Airportbooking.findByStatus", query = "SELECT a FROM Airportbooking a WHERE a.status = :status")})
public class Airportbooking implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bookingid")
    private Integer bookingid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "booking_date")
    @Temporal(TemporalType.DATE)
    private Date bookingDate;
    @Column(name = "nrofspaces")
    private Integer nrofspaces;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Size(max = 15)
    @Column(name = "anrede")
    private String anrede;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 90)
    @Column(name = "lastname")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 90)
    @Column(name = "forename")
    private String forename;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 30)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 127)
    @Column(name = "street")
    private String street;
    @Size(max = 15)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 30)
    @Column(name = "city")
    private String city;
    @Size(max = 15)
    @Column(name = "country")
    private String country;
    @Size(max = 127)
    @Column(name = "company")
    private String company;
    @Size(max = 127)
    @Column(name = "bankaccountholder")
    private String bankaccountholder;
    @Size(max = 63)
    @Column(name = "bank")
    private String bank;
    @Column(name = "bankaccount")
    private Integer bankaccount;
    @Column(name = "bankcode")
    private Integer bankcode;
    @Size(max = 34)
    @Column(name = "iban")
    private String iban;
    @Size(max = 15)
    @Column(name = "bic")
    private String bic;
    @Size(max = 15)
    @Column(name = "ust_idnr")
    private String ustIdnr;
    @Size(max = 100)
    @Column(name = "vehicle_licence")
    private String vehicleLicence;
    @Column(name = "person_nbr")
    private Integer personNbr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "departure")
    @Temporal(TemporalType.DATE)
    private Date departure;
    @Size(max = 5)
    @Column(name = "departure_hour")
    private String departureHour;
    @Size(max = 10)
    @Column(name = "departure_flight_nbr")
    private String departureFlightNbr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "arrival")
    @Temporal(TemporalType.DATE)
    private Date arrival;
    @Size(max = 5)
    @Column(name = "arrival_hour")
    private String arrivalHour;
    @Size(max = 10)
    @Column(name = "arrival_flight_nbr")
    private String arrivalFlightNbr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "contract_nbr")
    private String contractNbr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contract_track")
    private int contractTrack;
    @Size(max = 2147483647)
    @Column(name = "note")
    private String note;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Airportbooking() {
    }

    public Airportbooking(Integer bookingid) {
        this.bookingid = bookingid;
    }

    public Airportbooking(Integer bookingid, Date bookingDate, String lastname, String forename, String email, Date departure, Date arrival, String contractNbr, int contractTrack, String status) {
        this.bookingid = bookingid;
        this.bookingDate = bookingDate;
        this.lastname = lastname;
        this.forename = forename;
        this.email = email;
        this.departure = departure;
        this.arrival = arrival;
        this.contractNbr = contractNbr;
        this.contractTrack = contractTrack;
        this.status = status;
    }

    public Integer getBookingid() {
        return bookingid;
    }

    public void setBookingid(Integer bookingid) {
        this.bookingid = bookingid;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Integer getNrofspaces() {
        return nrofspaces;
    }

    public void setNrofspaces(Integer nrofspaces) {
        this.nrofspaces = nrofspaces;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBankaccountholder() {
        return bankaccountholder;
    }

    public void setBankaccountholder(String bankaccountholder) {
        this.bankaccountholder = bankaccountholder;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Integer getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(Integer bankaccount) {
        this.bankaccount = bankaccount;
    }

    public Integer getBankcode() {
        return bankcode;
    }

    public void setBankcode(Integer bankcode) {
        this.bankcode = bankcode;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getUstIdnr() {
        return ustIdnr;
    }

    public void setUstIdnr(String ustIdnr) {
        this.ustIdnr = ustIdnr;
    }

    public String getVehicleLicence() {
        return vehicleLicence;
    }

    public void setVehicleLicence(String vehicleLicence) {
        this.vehicleLicence = vehicleLicence;
    }

    public Integer getPersonNbr() {
        return personNbr;
    }

    public void setPersonNbr(Integer personNbr) {
        this.personNbr = personNbr;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public String getDepartureHour() {
        return departureHour;
    }

    public void setDepartureHour(String departureHour) {
        this.departureHour = departureHour;
    }

    public String getDepartureFlightNbr() {
        return departureFlightNbr;
    }

    public void setDepartureFlightNbr(String departureFlightNbr) {
        this.departureFlightNbr = departureFlightNbr;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public String getArrivalHour() {
        return arrivalHour;
    }

    public void setArrivalHour(String arrivalHour) {
        this.arrivalHour = arrivalHour;
    }

    public String getArrivalFlightNbr() {
        return arrivalFlightNbr;
    }

    public void setArrivalFlightNbr(String arrivalFlightNbr) {
        this.arrivalFlightNbr = arrivalFlightNbr;
    }

    public String getContractNbr() {
        return contractNbr;
    }

    public void setContractNbr(String contractNbr) {
        this.contractNbr = contractNbr;
    }

    public int getContractTrack() {
        return contractTrack;
    }

    public void setContractTrack(int contractTrack) {
        this.contractTrack = contractTrack;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookingid != null ? bookingid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Airportbooking)) {
            return false;
        }
        Airportbooking other = (Airportbooking) object;
        if ((this.bookingid == null && other.bookingid != null) || (this.bookingid != null && !this.bookingid.equals(other.bookingid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Airportbooking[ bookingid=" + bookingid + " ]";
    }
    
}
