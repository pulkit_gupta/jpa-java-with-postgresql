/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "allowedvehicle")
@NamedQueries({
    @NamedQuery(name = "Allowedvehicle.findAll", query = "SELECT a FROM Allowedvehicle a"),
    @NamedQuery(name = "Allowedvehicle.findByVehicleid", query = "SELECT a FROM Allowedvehicle a WHERE a.vehicleid = :vehicleid"),
    @NamedQuery(name = "Allowedvehicle.findByVehicle", query = "SELECT a FROM Allowedvehicle a WHERE a.vehicle = :vehicle"),
    @NamedQuery(name = "Allowedvehicle.findByPriceHour", query = "SELECT a FROM Allowedvehicle a WHERE a.priceHour = :priceHour"),
    @NamedQuery(name = "Allowedvehicle.findByPriceDay", query = "SELECT a FROM Allowedvehicle a WHERE a.priceDay = :priceDay"),
    @NamedQuery(name = "Allowedvehicle.findByPriceWeek", query = "SELECT a FROM Allowedvehicle a WHERE a.priceWeek = :priceWeek"),
    @NamedQuery(name = "Allowedvehicle.findByPriceMonth", query = "SELECT a FROM Allowedvehicle a WHERE a.priceMonth = :priceMonth"),
    @NamedQuery(name = "Allowedvehicle.findByStatus", query = "SELECT a FROM Allowedvehicle a WHERE a.status = :status")})
public class Allowedvehicle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vehicleid")
    private Integer vehicleid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "vehicle")
    private String vehicle;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_hour")
    private Double priceHour;
    @Column(name = "price_day")
    private Double priceDay;
    @Column(name = "price_week")
    private Double priceWeek;
    @Column(name = "price_month")
    private Double priceMonth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Allowedvehicle() {
    }

    public Allowedvehicle(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public Allowedvehicle(Integer vehicleid, String vehicle, String status) {
        this.vehicleid = vehicleid;
        this.vehicle = vehicle;
        this.status = status;
    }

    public Integer getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(Integer vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Double getPriceHour() {
        return priceHour;
    }

    public void setPriceHour(Double priceHour) {
        this.priceHour = priceHour;
    }

    public Double getPriceDay() {
        return priceDay;
    }

    public void setPriceDay(Double priceDay) {
        this.priceDay = priceDay;
    }

    public Double getPriceWeek() {
        return priceWeek;
    }

    public void setPriceWeek(Double priceWeek) {
        this.priceWeek = priceWeek;
    }

    public Double getPriceMonth() {
        return priceMonth;
    }

    public void setPriceMonth(Double priceMonth) {
        this.priceMonth = priceMonth;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleid != null ? vehicleid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Allowedvehicle)) {
            return false;
        }
        Allowedvehicle other = (Allowedvehicle) object;
        if ((this.vehicleid == null && other.vehicleid != null) || (this.vehicleid != null && !this.vehicleid.equals(other.vehicleid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Allowedvehicle[ vehicleid=" + vehicleid + " ]";
    }
    
}
