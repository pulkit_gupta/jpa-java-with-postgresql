/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "parkhaus")
@NamedQueries({
    @NamedQuery(name = "Parkhaus.findAll", query = "SELECT p FROM Parkhaus p"),
    @NamedQuery(name = "Parkhaus.findByParkhausid", query = "SELECT p FROM Parkhaus p WHERE p.parkhausid = :parkhausid"),
    @NamedQuery(name = "Parkhaus.findByUserid", query = "SELECT p FROM Parkhaus p WHERE p.userid = :userid"),
    @NamedQuery(name = "Parkhaus.findByLatitude", query = "SELECT p FROM Parkhaus p WHERE p.latitude = :latitude"),
    @NamedQuery(name = "Parkhaus.findByLongitude", query = "SELECT p FROM Parkhaus p WHERE p.longitude = :longitude"),
    @NamedQuery(name = "Parkhaus.findByStreet", query = "SELECT p FROM Parkhaus p WHERE p.street = :street"),
    @NamedQuery(name = "Parkhaus.findByZipcode", query = "SELECT p FROM Parkhaus p WHERE p.zipcode = :zipcode"),
    @NamedQuery(name = "Parkhaus.findByCity", query = "SELECT p FROM Parkhaus p WHERE p.city = :city"),
    @NamedQuery(name = "Parkhaus.findByCountry", query = "SELECT p FROM Parkhaus p WHERE p.country = :country"),
    @NamedQuery(name = "Parkhaus.findByStartDate", query = "SELECT p FROM Parkhaus p WHERE p.startDate = :startDate"),
    @NamedQuery(name = "Parkhaus.findByEndDate", query = "SELECT p FROM Parkhaus p WHERE p.endDate = :endDate"),
    @NamedQuery(name = "Parkhaus.findByAreatype", query = "SELECT p FROM Parkhaus p WHERE p.areatype = :areatype"),
    @NamedQuery(name = "Parkhaus.findByScheduletype", query = "SELECT p FROM Parkhaus p WHERE p.scheduletype = :scheduletype"),
    @NamedQuery(name = "Parkhaus.findByStatus", query = "SELECT p FROM Parkhaus p WHERE p.status = :status"),
    @NamedQuery(name = "Parkhaus.findByDescription", query = "SELECT p FROM Parkhaus p WHERE p.description = :description"),
    @NamedQuery(name = "Parkhaus.findBySecret", query = "SELECT p FROM Parkhaus p WHERE p.secret = :secret"),
    @NamedQuery(name = "Parkhaus.findByFoto1", query = "SELECT p FROM Parkhaus p WHERE p.foto1 = :foto1"),
    @NamedQuery(name = "Parkhaus.findByFoto2", query = "SELECT p FROM Parkhaus p WHERE p.foto2 = :foto2"),
    @NamedQuery(name = "Parkhaus.findByPriceMin", query = "SELECT p FROM Parkhaus p WHERE p.priceMin = :priceMin"),
    @NamedQuery(name = "Parkhaus.findByNrofspaces", query = "SELECT p FROM Parkhaus p WHERE p.nrofspaces = :nrofspaces"),
    @NamedQuery(name = "Parkhaus.findByMarkeruri", query = "SELECT p FROM Parkhaus p WHERE p.markeruri = :markeruri"),
    @NamedQuery(name = "Parkhaus.findByTitle", query = "SELECT p FROM Parkhaus p WHERE p.title = :title"),
    @NamedQuery(name = "Parkhaus.findByFoto3", query = "SELECT p FROM Parkhaus p WHERE p.foto3 = :foto3"),
    @NamedQuery(name = "Parkhaus.findByPricemodel", query = "SELECT p FROM Parkhaus p WHERE p.pricemodel = :pricemodel"),
    @NamedQuery(name = "Parkhaus.findBySeoTitle", query = "SELECT p FROM Parkhaus p WHERE p.seoTitle = :seoTitle"),
    @NamedQuery(name = "Parkhaus.findByCitySeo", query = "SELECT p FROM Parkhaus p WHERE p.citySeo = :citySeo"),
    @NamedQuery(name = "Parkhaus.findByOpeningTime", query = "SELECT p FROM Parkhaus p WHERE p.openingTime = :openingTime"),
    @NamedQuery(name = "Parkhaus.findByHeadroom", query = "SELECT p FROM Parkhaus p WHERE p.headroom = :headroom"),
    @NamedQuery(name = "Parkhaus.findByOperator", query = "SELECT p FROM Parkhaus p WHERE p.operator = :operator"),
    @NamedQuery(name = "Parkhaus.findByFoto1Title", query = "SELECT p FROM Parkhaus p WHERE p.foto1Title = :foto1Title"),
    @NamedQuery(name = "Parkhaus.findByFoto2Title", query = "SELECT p FROM Parkhaus p WHERE p.foto2Title = :foto2Title"),
    @NamedQuery(name = "Parkhaus.findByFoto3Title", query = "SELECT p FROM Parkhaus p WHERE p.foto3Title = :foto3Title"),
    @NamedQuery(name = "Parkhaus.findByFoto4", query = "SELECT p FROM Parkhaus p WHERE p.foto4 = :foto4"),
    @NamedQuery(name = "Parkhaus.findByFoto4Title", query = "SELECT p FROM Parkhaus p WHERE p.foto4Title = :foto4Title"),
    @NamedQuery(name = "Parkhaus.findByFoto5", query = "SELECT p FROM Parkhaus p WHERE p.foto5 = :foto5"),
    @NamedQuery(name = "Parkhaus.findByFoto5Title", query = "SELECT p FROM Parkhaus p WHERE p.foto5Title = :foto5Title"),
    @NamedQuery(name = "Parkhaus.findByLongterm", query = "SELECT p FROM Parkhaus p WHERE p.longterm = :longterm")})
public class Parkhaus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "parkhausid")
    private Integer parkhausid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userid")
    private int userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private double longitude;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 127)
    @Column(name = "street")
    private String street;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "zipcode")
    private String zipcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 63)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Size(max = 63)
    @Column(name = "areatype")
    private String areatype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 63)
    @Column(name = "scheduletype")
    private String scheduletype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Size(max = 2147483647)
    @Column(name = "secret")
    private String secret;
    @Size(max = 255)
    @Column(name = "foto1")
    private String foto1;
    @Size(max = 255)
    @Column(name = "foto2")
    private String foto2;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_min")
    private Double priceMin;
    @Column(name = "nrofspaces")
    private Integer nrofspaces;
    @Size(max = 255)
    @Column(name = "markeruri")
    private String markeruri;
    @Size(max = 120)
    @Column(name = "title")
    private String title;
    @Size(max = 255)
    @Column(name = "foto3")
    private String foto3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "pricemodel")
    private String pricemodel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 130)
    @Column(name = "seo_title")
    private String seoTitle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 63)
    @Column(name = "city_seo")
    private String citySeo;
    @Size(max = 2147483647)
    @Column(name = "opening_time")
    private String openingTime;
    @Size(max = 41)
    @Column(name = "headroom")
    private String headroom;
    @Size(max = 100)
    @Column(name = "operator")
    private String operator;
    @Size(max = 130)
    @Column(name = "foto1_title")
    private String foto1Title;
    @Size(max = 130)
    @Column(name = "foto2_title")
    private String foto2Title;
    @Size(max = 130)
    @Column(name = "foto3_title")
    private String foto3Title;
    @Size(max = 255)
    @Column(name = "foto4")
    private String foto4;
    @Size(max = 130)
    @Column(name = "foto4_title")
    private String foto4Title;
    @Size(max = 255)
    @Column(name = "foto5")
    private String foto5;
    @Size(max = 130)
    @Column(name = "foto5_title")
    private String foto5Title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "longterm")
    private String longterm;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkhausid")
    private List<Carparkpricing1> carparkpricing1List;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkhausid")
    private List<Parkareaday1> parkareaday1List;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkhausid")
    private List<Carparkservice1> carparkservice1List;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkhausid")
    private List<Carparkfacility1> carparkfacility1List;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parkhausid")
    private List<Areacontact1> areacontact1List;

    public Parkhaus() {
    }

    public Parkhaus(Integer parkhausid) {
        this.parkhausid = parkhausid;
    }

    public Parkhaus(Integer parkhausid, int userid, double latitude, double longitude, String street, String zipcode, String city, String country, Date startDate, Date endDate, String scheduletype, String status, String pricemodel, String seoTitle, String citySeo, String longterm) {
        this.parkhausid = parkhausid;
        this.userid = userid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.street = street;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
        this.startDate = startDate;
        this.endDate = endDate;
        this.scheduletype = scheduletype;
        this.status = status;
        this.pricemodel = pricemodel;
        this.seoTitle = seoTitle;
        this.citySeo = citySeo;
        this.longterm = longterm;
    }

    public Integer getParkhausid() {
        return parkhausid;
    }

    public void setParkhausid(Integer parkhausid) {
        this.parkhausid = parkhausid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getAreatype() {
        return areatype;
    }

    public void setAreatype(String areatype) {
        this.areatype = areatype;
    }

    public String getScheduletype() {
        return scheduletype;
    }

    public void setScheduletype(String scheduletype) {
        this.scheduletype = scheduletype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public Double getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Double priceMin) {
        this.priceMin = priceMin;
    }

    public Integer getNrofspaces() {
        return nrofspaces;
    }

    public void setNrofspaces(Integer nrofspaces) {
        this.nrofspaces = nrofspaces;
    }

    public String getMarkeruri() {
        return markeruri;
    }

    public void setMarkeruri(String markeruri) {
        this.markeruri = markeruri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFoto3() {
        return foto3;
    }

    public void setFoto3(String foto3) {
        this.foto3 = foto3;
    }

    public String getPricemodel() {
        return pricemodel;
    }

    public void setPricemodel(String pricemodel) {
        this.pricemodel = pricemodel;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getCitySeo() {
        return citySeo;
    }

    public void setCitySeo(String citySeo) {
        this.citySeo = citySeo;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getHeadroom() {
        return headroom;
    }

    public void setHeadroom(String headroom) {
        this.headroom = headroom;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFoto1Title() {
        return foto1Title;
    }

    public void setFoto1Title(String foto1Title) {
        this.foto1Title = foto1Title;
    }

    public String getFoto2Title() {
        return foto2Title;
    }

    public void setFoto2Title(String foto2Title) {
        this.foto2Title = foto2Title;
    }

    public String getFoto3Title() {
        return foto3Title;
    }

    public void setFoto3Title(String foto3Title) {
        this.foto3Title = foto3Title;
    }

    public String getFoto4() {
        return foto4;
    }

    public void setFoto4(String foto4) {
        this.foto4 = foto4;
    }

    public String getFoto4Title() {
        return foto4Title;
    }

    public void setFoto4Title(String foto4Title) {
        this.foto4Title = foto4Title;
    }

    public String getFoto5() {
        return foto5;
    }

    public void setFoto5(String foto5) {
        this.foto5 = foto5;
    }

    public String getFoto5Title() {
        return foto5Title;
    }

    public void setFoto5Title(String foto5Title) {
        this.foto5Title = foto5Title;
    }

    public String getLongterm() {
        return longterm;
    }

    public void setLongterm(String longterm) {
        this.longterm = longterm;
    }

    public List<Carparkpricing1> getCarparkpricing1List() {
        return carparkpricing1List;
    }

    public void setCarparkpricing1List(List<Carparkpricing1> carparkpricing1List) {
        this.carparkpricing1List = carparkpricing1List;
    }

    public List<Parkareaday1> getParkareaday1List() {
        return parkareaday1List;
    }

    public void setParkareaday1List(List<Parkareaday1> parkareaday1List) {
        this.parkareaday1List = parkareaday1List;
    }

    public List<Carparkservice1> getCarparkservice1List() {
        return carparkservice1List;
    }

    public void setCarparkservice1List(List<Carparkservice1> carparkservice1List) {
        this.carparkservice1List = carparkservice1List;
    }

    public List<Carparkfacility1> getCarparkfacility1List() {
        return carparkfacility1List;
    }

    public void setCarparkfacility1List(List<Carparkfacility1> carparkfacility1List) {
        this.carparkfacility1List = carparkfacility1List;
    }

    public List<Areacontact1> getAreacontact1List() {
        return areacontact1List;
    }

    public void setAreacontact1List(List<Areacontact1> areacontact1List) {
        this.areacontact1List = areacontact1List;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parkhausid != null ? parkhausid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parkhaus)) {
            return false;
        }
        Parkhaus other = (Parkhaus) object;
        if ((this.parkhausid == null && other.parkhausid != null) || (this.parkhausid != null && !this.parkhausid.equals(other.parkhausid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Parkhaus[ parkhausid=" + parkhausid + " ]";
    }
    
}
