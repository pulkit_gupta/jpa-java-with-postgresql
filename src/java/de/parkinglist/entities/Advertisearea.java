/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "advertisearea")
@NamedQueries({
    @NamedQuery(name = "Advertisearea.findAll", query = "SELECT a FROM Advertisearea a"),
    @NamedQuery(name = "Advertisearea.findByAdsareaid", query = "SELECT a FROM Advertisearea a WHERE a.adsareaid = :adsareaid"),
    @NamedQuery(name = "Advertisearea.findByParkareaid", query = "SELECT a FROM Advertisearea a WHERE a.parkareaid = :parkareaid"),
    @NamedQuery(name = "Advertisearea.findByTitleBold", query = "SELECT a FROM Advertisearea a WHERE a.titleBold = :titleBold"),
    @NamedQuery(name = "Advertisearea.findByBackground", query = "SELECT a FROM Advertisearea a WHERE a.background = :background"),
    @NamedQuery(name = "Advertisearea.findByMarkerBig", query = "SELECT a FROM Advertisearea a WHERE a.markerBig = :markerBig"),
    @NamedQuery(name = "Advertisearea.findByPlaceTop", query = "SELECT a FROM Advertisearea a WHERE a.placeTop = :placeTop"),
    @NamedQuery(name = "Advertisearea.findBySettingDate", query = "SELECT a FROM Advertisearea a WHERE a.settingDate = :settingDate"),
    @NamedQuery(name = "Advertisearea.findByStartDate", query = "SELECT a FROM Advertisearea a WHERE a.startDate = :startDate"),
    @NamedQuery(name = "Advertisearea.findByEndDate", query = "SELECT a FROM Advertisearea a WHERE a.endDate = :endDate"),
    @NamedQuery(name = "Advertisearea.findByStatus", query = "SELECT a FROM Advertisearea a WHERE a.status = :status")})
public class Advertisearea implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "adsareaid")
    private Integer adsareaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "parkareaid")
    private int parkareaid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "title_bold")
    private String titleBold;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "background")
    private String background;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "marker_big")
    private String markerBig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "place_top")
    private String placeTop;
    @Column(name = "setting_date")
    @Temporal(TemporalType.DATE)
    private Date settingDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @ManyToOne(optional = false)
    private Parkuser userid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adsareaid")
    private List<Invoice> invoiceList;

    public Advertisearea() {
    }

    public Advertisearea(Integer adsareaid) {
        this.adsareaid = adsareaid;
    }

    public Advertisearea(Integer adsareaid, int parkareaid, String titleBold, String background, String markerBig, String placeTop, Date startDate, Date endDate, String status) {
        this.adsareaid = adsareaid;
        this.parkareaid = parkareaid;
        this.titleBold = titleBold;
        this.background = background;
        this.markerBig = markerBig;
        this.placeTop = placeTop;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    public Integer getAdsareaid() {
        return adsareaid;
    }

    public void setAdsareaid(Integer adsareaid) {
        this.adsareaid = adsareaid;
    }

    public int getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(int parkareaid) {
        this.parkareaid = parkareaid;
    }

    public String getTitleBold() {
        return titleBold;
    }

    public void setTitleBold(String titleBold) {
        this.titleBold = titleBold;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getMarkerBig() {
        return markerBig;
    }

    public void setMarkerBig(String markerBig) {
        this.markerBig = markerBig;
    }

    public String getPlaceTop() {
        return placeTop;
    }

    public void setPlaceTop(String placeTop) {
        this.placeTop = placeTop;
    }

    public Date getSettingDate() {
        return settingDate;
    }

    public void setSettingDate(Date settingDate) {
        this.settingDate = settingDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkuser getUserid() {
        return userid;
    }

    public void setUserid(Parkuser userid) {
        this.userid = userid;
    }

    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adsareaid != null ? adsareaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Advertisearea)) {
            return false;
        }
        Advertisearea other = (Advertisearea) object;
        if ((this.adsareaid == null && other.adsareaid != null) || (this.adsareaid != null && !this.adsareaid.equals(other.adsareaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Advertisearea[ adsareaid=" + adsareaid + " ]";
    }
    
}
