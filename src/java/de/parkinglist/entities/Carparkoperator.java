/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkoperator")
@NamedQueries({
    @NamedQuery(name = "Carparkoperator.findAll", query = "SELECT c FROM Carparkoperator c"),
    @NamedQuery(name = "Carparkoperator.findByCarparkid", query = "SELECT c FROM Carparkoperator c WHERE c.carparkid = :carparkid"),
    @NamedQuery(name = "Carparkoperator.findByName", query = "SELECT c FROM Carparkoperator c WHERE c.name = :name"),
    @NamedQuery(name = "Carparkoperator.findByStreet", query = "SELECT c FROM Carparkoperator c WHERE c.street = :street"),
    @NamedQuery(name = "Carparkoperator.findByZipcode", query = "SELECT c FROM Carparkoperator c WHERE c.zipcode = :zipcode"),
    @NamedQuery(name = "Carparkoperator.findByCity", query = "SELECT c FROM Carparkoperator c WHERE c.city = :city"),
    @NamedQuery(name = "Carparkoperator.findByCountry", query = "SELECT c FROM Carparkoperator c WHERE c.country = :country"),
    @NamedQuery(name = "Carparkoperator.findByEmail", query = "SELECT c FROM Carparkoperator c WHERE c.email = :email"),
    @NamedQuery(name = "Carparkoperator.findByPhone", query = "SELECT c FROM Carparkoperator c WHERE c.phone = :phone"),
    @NamedQuery(name = "Carparkoperator.findByMobile", query = "SELECT c FROM Carparkoperator c WHERE c.mobile = :mobile"),
    @NamedQuery(name = "Carparkoperator.findByFax", query = "SELECT c FROM Carparkoperator c WHERE c.fax = :fax"),
    @NamedQuery(name = "Carparkoperator.findByDescription", query = "SELECT c FROM Carparkoperator c WHERE c.description = :description"),
    @NamedQuery(name = "Carparkoperator.findByBankaccountholder", query = "SELECT c FROM Carparkoperator c WHERE c.bankaccountholder = :bankaccountholder"),
    @NamedQuery(name = "Carparkoperator.findByIban", query = "SELECT c FROM Carparkoperator c WHERE c.iban = :iban"),
    @NamedQuery(name = "Carparkoperator.findByBic", query = "SELECT c FROM Carparkoperator c WHERE c.bic = :bic"),
    @NamedQuery(name = "Carparkoperator.findByStatus", query = "SELECT c FROM Carparkoperator c WHERE c.status = :status"),
    @NamedQuery(name = "Carparkoperator.findByAgb", query = "SELECT c FROM Carparkoperator c WHERE c.agb = :agb"),
    @NamedQuery(name = "Carparkoperator.findByUstIdnr", query = "SELECT c FROM Carparkoperator c WHERE c.ustIdnr = :ustIdnr"),
    @NamedQuery(name = "Carparkoperator.findByLogo1", query = "SELECT c FROM Carparkoperator c WHERE c.logo1 = :logo1"),
    @NamedQuery(name = "Carparkoperator.findByLeadtime", query = "SELECT c FROM Carparkoperator c WHERE c.leadtime = :leadtime")})
public class Carparkoperator implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "carparkid")
    private Integer carparkid;
    @Size(max = 127)
    @Column(name = "name")
    private String name;
    @Size(max = 127)
    @Column(name = "street")
    private String street;
    @Size(max = 31)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 63)
    @Column(name = "city")
    private String city;
    @Size(max = 3)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 127)
    @Column(name = "email")
    private String email;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "phone")
    private String phone;
    @Size(max = 31)
    @Column(name = "mobile")
    private String mobile;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 63)
    @Column(name = "fax")
    private String fax;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Size(max = 127)
    @Column(name = "bankaccountholder")
    private String bankaccountholder;
    @Size(max = 31)
    @Column(name = "iban")
    private String iban;
    @Size(max = 15)
    @Column(name = "bic")
    private String bic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;
    @Size(max = 2147483647)
    @Column(name = "agb")
    private String agb;
    @Size(max = 15)
    @Column(name = "ust_idnr")
    private String ustIdnr;
    @Size(max = 60)
    @Column(name = "logo1")
    private String logo1;
    @Column(name = "leadtime")
    private Integer leadtime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carparkid")
    private List<Operatorservice> operatorserviceList;

    public Carparkoperator() {
    }

    public Carparkoperator(Integer carparkid) {
        this.carparkid = carparkid;
    }

    public Carparkoperator(Integer carparkid, String status) {
        this.carparkid = carparkid;
        this.status = status;
    }

    public Integer getCarparkid() {
        return carparkid;
    }

    public void setCarparkid(Integer carparkid) {
        this.carparkid = carparkid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBankaccountholder() {
        return bankaccountholder;
    }

    public void setBankaccountholder(String bankaccountholder) {
        this.bankaccountholder = bankaccountholder;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgb() {
        return agb;
    }

    public void setAgb(String agb) {
        this.agb = agb;
    }

    public String getUstIdnr() {
        return ustIdnr;
    }

    public void setUstIdnr(String ustIdnr) {
        this.ustIdnr = ustIdnr;
    }

    public String getLogo1() {
        return logo1;
    }

    public void setLogo1(String logo1) {
        this.logo1 = logo1;
    }

    public Integer getLeadtime() {
        return leadtime;
    }

    public void setLeadtime(Integer leadtime) {
        this.leadtime = leadtime;
    }

    public List<Operatorservice> getOperatorserviceList() {
        return operatorserviceList;
    }

    public void setOperatorserviceList(List<Operatorservice> operatorserviceList) {
        this.operatorserviceList = operatorserviceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carparkid != null ? carparkid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkoperator)) {
            return false;
        }
        Carparkoperator other = (Carparkoperator) object;
        if ((this.carparkid == null && other.carparkid != null) || (this.carparkid != null && !this.carparkid.equals(other.carparkid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkoperator[ carparkid=" + carparkid + " ]";
    }
    
}
