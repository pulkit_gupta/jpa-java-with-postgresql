/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "operatorservice")
@NamedQueries({
    @NamedQuery(name = "Operatorservice.findAll", query = "SELECT o FROM Operatorservice o"),
    @NamedQuery(name = "Operatorservice.findByOpserviceid", query = "SELECT o FROM Operatorservice o WHERE o.opserviceid = :opserviceid"),
    @NamedQuery(name = "Operatorservice.findByTitle", query = "SELECT o FROM Operatorservice o WHERE o.title = :title"),
    @NamedQuery(name = "Operatorservice.findByDescript", query = "SELECT o FROM Operatorservice o WHERE o.descript = :descript"),
    @NamedQuery(name = "Operatorservice.findByPrice", query = "SELECT o FROM Operatorservice o WHERE o.price = :price"),
    @NamedQuery(name = "Operatorservice.findByStatus", query = "SELECT o FROM Operatorservice o WHERE o.status = :status")})
public class Operatorservice implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "opserviceid")
    private Integer opserviceid;
    @Size(max = 100)
    @Column(name = "title")
    private String title;
    @Size(max = 2147483647)
    @Column(name = "descript")
    private String descript;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "carparkid", referencedColumnName = "carparkid")
    @ManyToOne(optional = false)
    private Carparkoperator carparkid;

    public Operatorservice() {
    }

    public Operatorservice(Integer opserviceid) {
        this.opserviceid = opserviceid;
    }

    public Operatorservice(Integer opserviceid, String status) {
        this.opserviceid = opserviceid;
        this.status = status;
    }

    public Integer getOpserviceid() {
        return opserviceid;
    }

    public void setOpserviceid(Integer opserviceid) {
        this.opserviceid = opserviceid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Carparkoperator getCarparkid() {
        return carparkid;
    }

    public void setCarparkid(Carparkoperator carparkid) {
        this.carparkid = carparkid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (opserviceid != null ? opserviceid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operatorservice)) {
            return false;
        }
        Operatorservice other = (Operatorservice) object;
        if ((this.opserviceid == null && other.opserviceid != null) || (this.opserviceid != null && !this.opserviceid.equals(other.opserviceid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Operatorservice[ opserviceid=" + opserviceid + " ]";
    }
    
}
