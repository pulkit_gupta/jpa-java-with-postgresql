/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "fotos")
@NamedQueries({
    @NamedQuery(name = "Fotos.findAll", query = "SELECT f FROM Fotos f"),
    @NamedQuery(name = "Fotos.findByFotoid", query = "SELECT f FROM Fotos f WHERE f.fotoid = :fotoid"),
    @NamedQuery(name = "Fotos.findByFoto1", query = "SELECT f FROM Fotos f WHERE f.foto1 = :foto1"),
    @NamedQuery(name = "Fotos.findByFoto2", query = "SELECT f FROM Fotos f WHERE f.foto2 = :foto2"),
    @NamedQuery(name = "Fotos.findByFoto3", query = "SELECT f FROM Fotos f WHERE f.foto3 = :foto3"),
    @NamedQuery(name = "Fotos.findByFoto4", query = "SELECT f FROM Fotos f WHERE f.foto4 = :foto4"),
    @NamedQuery(name = "Fotos.findByFoto5", query = "SELECT f FROM Fotos f WHERE f.foto5 = :foto5"),
    @NamedQuery(name = "Fotos.findByFoto1Title", query = "SELECT f FROM Fotos f WHERE f.foto1Title = :foto1Title"),
    @NamedQuery(name = "Fotos.findByFoto2Title", query = "SELECT f FROM Fotos f WHERE f.foto2Title = :foto2Title"),
    @NamedQuery(name = "Fotos.findByFoto3Title", query = "SELECT f FROM Fotos f WHERE f.foto3Title = :foto3Title"),
    @NamedQuery(name = "Fotos.findByFoto4Title", query = "SELECT f FROM Fotos f WHERE f.foto4Title = :foto4Title"),
    @NamedQuery(name = "Fotos.findByFoto5Title", query = "SELECT f FROM Fotos f WHERE f.foto5Title = :foto5Title"),
    @NamedQuery(name = "Fotos.findByLogo1", query = "SELECT f FROM Fotos f WHERE f.logo1 = :logo1"),
    @NamedQuery(name = "Fotos.findByLogo2", query = "SELECT f FROM Fotos f WHERE f.logo2 = :logo2")})
public class Fotos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "fotoid")
    private Integer fotoid;
    @Size(max = 150)
    @Column(name = "foto1")
    private String foto1;
    @Size(max = 150)
    @Column(name = "foto2")
    private String foto2;
    @Size(max = 150)
    @Column(name = "foto3")
    private String foto3;
    @Size(max = 150)
    @Column(name = "foto4")
    private String foto4;
    @Size(max = 150)
    @Column(name = "foto5")
    private String foto5;
    @Size(max = 130)
    @Column(name = "foto1_title")
    private String foto1Title;
    @Size(max = 130)
    @Column(name = "foto2_title")
    private String foto2Title;
    @Size(max = 130)
    @Column(name = "foto3_title")
    private String foto3Title;
    @Size(max = 130)
    @Column(name = "foto4_title")
    private String foto4Title;
    @Size(max = 130)
    @Column(name = "foto5_title")
    private String foto5Title;
    @Size(max = 60)
    @Column(name = "logo1")
    private String logo1;
    @Size(max = 60)
    @Column(name = "logo2")
    private String logo2;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Fotos() {
    }

    public Fotos(Integer fotoid) {
        this.fotoid = fotoid;
    }

    public Integer getFotoid() {
        return fotoid;
    }

    public void setFotoid(Integer fotoid) {
        this.fotoid = fotoid;
    }

    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public String getFoto3() {
        return foto3;
    }

    public void setFoto3(String foto3) {
        this.foto3 = foto3;
    }

    public String getFoto4() {
        return foto4;
    }

    public void setFoto4(String foto4) {
        this.foto4 = foto4;
    }

    public String getFoto5() {
        return foto5;
    }

    public void setFoto5(String foto5) {
        this.foto5 = foto5;
    }

    public String getFoto1Title() {
        return foto1Title;
    }

    public void setFoto1Title(String foto1Title) {
        this.foto1Title = foto1Title;
    }

    public String getFoto2Title() {
        return foto2Title;
    }

    public void setFoto2Title(String foto2Title) {
        this.foto2Title = foto2Title;
    }

    public String getFoto3Title() {
        return foto3Title;
    }

    public void setFoto3Title(String foto3Title) {
        this.foto3Title = foto3Title;
    }

    public String getFoto4Title() {
        return foto4Title;
    }

    public void setFoto4Title(String foto4Title) {
        this.foto4Title = foto4Title;
    }

    public String getFoto5Title() {
        return foto5Title;
    }

    public void setFoto5Title(String foto5Title) {
        this.foto5Title = foto5Title;
    }

    public String getLogo1() {
        return logo1;
    }

    public void setLogo1(String logo1) {
        this.logo1 = logo1;
    }

    public String getLogo2() {
        return logo2;
    }

    public void setLogo2(String logo2) {
        this.logo2 = logo2;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fotoid != null ? fotoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fotos)) {
            return false;
        }
        Fotos other = (Fotos) object;
        if ((this.fotoid == null && other.fotoid != null) || (this.fotoid != null && !this.fotoid.equals(other.fotoid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Fotos[ fotoid=" + fotoid + " ]";
    }
    
}
