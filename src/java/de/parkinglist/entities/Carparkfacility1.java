/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkfacility1")
@NamedQueries({
    @NamedQuery(name = "Carparkfacility1.findAll", query = "SELECT c FROM Carparkfacility1 c"),
    @NamedQuery(name = "Carparkfacility1.findByCpfacility1id", query = "SELECT c FROM Carparkfacility1 c WHERE c.cpfacility1id = :cpfacility1id"),
    @NamedQuery(name = "Carparkfacility1.findByFacility", query = "SELECT c FROM Carparkfacility1 c WHERE c.facility = :facility"),
    @NamedQuery(name = "Carparkfacility1.findByStatus", query = "SELECT c FROM Carparkfacility1 c WHERE c.status = :status")})
public class Carparkfacility1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cpfacility1id")
    private Integer cpfacility1id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "facility")
    private String facility;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkhausid", referencedColumnName = "parkhausid")
    @ManyToOne(optional = false)
    private Parkhaus parkhausid;

    public Carparkfacility1() {
    }

    public Carparkfacility1(Integer cpfacility1id) {
        this.cpfacility1id = cpfacility1id;
    }

    public Carparkfacility1(Integer cpfacility1id, String facility, String status) {
        this.cpfacility1id = cpfacility1id;
        this.facility = facility;
        this.status = status;
    }

    public Integer getCpfacility1id() {
        return cpfacility1id;
    }

    public void setCpfacility1id(Integer cpfacility1id) {
        this.cpfacility1id = cpfacility1id;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkhaus getParkhausid() {
        return parkhausid;
    }

    public void setParkhausid(Parkhaus parkhausid) {
        this.parkhausid = parkhausid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cpfacility1id != null ? cpfacility1id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkfacility1)) {
            return false;
        }
        Carparkfacility1 other = (Carparkfacility1) object;
        if ((this.cpfacility1id == null && other.cpfacility1id != null) || (this.cpfacility1id != null && !this.cpfacility1id.equals(other.cpfacility1id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkfacility1[ cpfacility1id=" + cpfacility1id + " ]";
    }
    
}
