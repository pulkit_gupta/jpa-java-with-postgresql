/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkpricing")
@NamedQueries({
    @NamedQuery(name = "Carparkpricing.findAll", query = "SELECT c FROM Carparkpricing c"),
    @NamedQuery(name = "Carparkpricing.findByCppricingid", query = "SELECT c FROM Carparkpricing c WHERE c.cppricingid = :cppricingid"),
    @NamedQuery(name = "Carparkpricing.findByPricing", query = "SELECT c FROM Carparkpricing c WHERE c.pricing = :pricing"),
    @NamedQuery(name = "Carparkpricing.findByStatus", query = "SELECT c FROM Carparkpricing c WHERE c.status = :status")})
public class Carparkpricing implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cppricingid")
    private Integer cppricingid;
    @Size(max = 2147483647)
    @Column(name = "pricing")
    private String pricing;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Carparkpricing() {
    }

    public Carparkpricing(Integer cppricingid) {
        this.cppricingid = cppricingid;
    }

    public Carparkpricing(Integer cppricingid, String status) {
        this.cppricingid = cppricingid;
        this.status = status;
    }

    public Integer getCppricingid() {
        return cppricingid;
    }

    public void setCppricingid(Integer cppricingid) {
        this.cppricingid = cppricingid;
    }

    public String getPricing() {
        return pricing;
    }

    public void setPricing(String pricing) {
        this.pricing = pricing;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cppricingid != null ? cppricingid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkpricing)) {
            return false;
        }
        Carparkpricing other = (Carparkpricing) object;
        if ((this.cppricingid == null && other.cppricingid != null) || (this.cppricingid != null && !this.cppricingid.equals(other.cppricingid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkpricing[ cppricingid=" + cppricingid + " ]";
    }
    
}
