/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "areafacility")
@NamedQueries({
    @NamedQuery(name = "Areafacility.findAll", query = "SELECT a FROM Areafacility a"),
    @NamedQuery(name = "Areafacility.findByFacilityid", query = "SELECT a FROM Areafacility a WHERE a.facilityid = :facilityid"),
    @NamedQuery(name = "Areafacility.findByFacility", query = "SELECT a FROM Areafacility a WHERE a.facility = :facility"),
    @NamedQuery(name = "Areafacility.findByStatus", query = "SELECT a FROM Areafacility a WHERE a.status = :status")})
public class Areafacility implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "facilityid")
    private Integer facilityid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "facility")
    private String facility;
    @Size(max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Areafacility() {
    }

    public Areafacility(Integer facilityid) {
        this.facilityid = facilityid;
    }

    public Areafacility(Integer facilityid, String facility) {
        this.facilityid = facilityid;
        this.facility = facility;
    }

    public Integer getFacilityid() {
        return facilityid;
    }

    public void setFacilityid(Integer facilityid) {
        this.facilityid = facilityid;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (facilityid != null ? facilityid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Areafacility)) {
            return false;
        }
        Areafacility other = (Areafacility) object;
        if ((this.facilityid == null && other.facilityid != null) || (this.facilityid != null && !this.facilityid.equals(other.facilityid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Areafacility[ facilityid=" + facilityid + " ]";
    }
    
}
