/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkfacility")
@NamedQueries({
    @NamedQuery(name = "Carparkfacility.findAll", query = "SELECT c FROM Carparkfacility c"),
    @NamedQuery(name = "Carparkfacility.findByCpfacilityid", query = "SELECT c FROM Carparkfacility c WHERE c.cpfacilityid = :cpfacilityid"),
    @NamedQuery(name = "Carparkfacility.findByFacility", query = "SELECT c FROM Carparkfacility c WHERE c.facility = :facility"),
    @NamedQuery(name = "Carparkfacility.findByStatus", query = "SELECT c FROM Carparkfacility c WHERE c.status = :status")})
public class Carparkfacility implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cpfacilityid")
    private Integer cpfacilityid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "facility")
    private String facility;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Carparkfacility() {
    }

    public Carparkfacility(Integer cpfacilityid) {
        this.cpfacilityid = cpfacilityid;
    }

    public Carparkfacility(Integer cpfacilityid, String facility, String status) {
        this.cpfacilityid = cpfacilityid;
        this.facility = facility;
        this.status = status;
    }

    public Integer getCpfacilityid() {
        return cpfacilityid;
    }

    public void setCpfacilityid(Integer cpfacilityid) {
        this.cpfacilityid = cpfacilityid;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cpfacilityid != null ? cpfacilityid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkfacility)) {
            return false;
        }
        Carparkfacility other = (Carparkfacility) object;
        if ((this.cpfacilityid == null && other.cpfacilityid != null) || (this.cpfacilityid != null && !this.cpfacilityid.equals(other.cpfacilityid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkfacility[ cpfacilityid=" + cpfacilityid + " ]";
    }
    
}
