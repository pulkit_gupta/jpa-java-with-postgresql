/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "proposal")
@NamedQueries({
    @NamedQuery(name = "Proposal.findAll", query = "SELECT p FROM Proposal p"),
    @NamedQuery(name = "Proposal.findByProposalid", query = "SELECT p FROM Proposal p WHERE p.proposalid = :proposalid"),
    @NamedQuery(name = "Proposal.findByProposal", query = "SELECT p FROM Proposal p WHERE p.proposal = :proposal"),
    @NamedQuery(name = "Proposal.findByTitle", query = "SELECT p FROM Proposal p WHERE p.title = :title"),
    @NamedQuery(name = "Proposal.findByLatitude", query = "SELECT p FROM Proposal p WHERE p.latitude = :latitude"),
    @NamedQuery(name = "Proposal.findByLongitude", query = "SELECT p FROM Proposal p WHERE p.longitude = :longitude"),
    @NamedQuery(name = "Proposal.findByStreet", query = "SELECT p FROM Proposal p WHERE p.street = :street"),
    @NamedQuery(name = "Proposal.findByZipcode", query = "SELECT p FROM Proposal p WHERE p.zipcode = :zipcode"),
    @NamedQuery(name = "Proposal.findByCity", query = "SELECT p FROM Proposal p WHERE p.city = :city"),
    @NamedQuery(name = "Proposal.findByCountry", query = "SELECT p FROM Proposal p WHERE p.country = :country"),
    @NamedQuery(name = "Proposal.findByAddress", query = "SELECT p FROM Proposal p WHERE p.address = :address"),
    @NamedQuery(name = "Proposal.findByDatum", query = "SELECT p FROM Proposal p WHERE p.datum = :datum"),
    @NamedQuery(name = "Proposal.findByVotes", query = "SELECT p FROM Proposal p WHERE p.votes = :votes"),
    @NamedQuery(name = "Proposal.findByStatus", query = "SELECT p FROM Proposal p WHERE p.status = :status")})
public class Proposal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "proposalid")
    private Integer proposalid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "proposal")
    private String proposal;
    @Size(max = 250)
    @Column(name = "title")
    private String title;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Size(max = 127)
    @Column(name = "street")
    private String street;
    @Size(max = 31)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 63)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datum")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;
    @Column(name = "votes")
    private Integer votes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proposalid")
    private List<Prpslcomment> prpslcommentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proposalid")
    private List<Uservote> uservoteList;
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @ManyToOne(optional = false)
    private Parkuser userid;

    public Proposal() {
    }

    public Proposal(Integer proposalid) {
        this.proposalid = proposalid;
    }

    public Proposal(Integer proposalid, String proposal, String country, String address, Date datum, String status) {
        this.proposalid = proposalid;
        this.proposal = proposal;
        this.country = country;
        this.address = address;
        this.datum = datum;
        this.status = status;
    }

    public Integer getProposalid() {
        return proposalid;
    }

    public void setProposalid(Integer proposalid) {
        this.proposalid = proposalid;
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Prpslcomment> getPrpslcommentList() {
        return prpslcommentList;
    }

    public void setPrpslcommentList(List<Prpslcomment> prpslcommentList) {
        this.prpslcommentList = prpslcommentList;
    }

    public List<Uservote> getUservoteList() {
        return uservoteList;
    }

    public void setUservoteList(List<Uservote> uservoteList) {
        this.uservoteList = uservoteList;
    }

    public Parkuser getUserid() {
        return userid;
    }

    public void setUserid(Parkuser userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proposalid != null ? proposalid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proposal)) {
            return false;
        }
        Proposal other = (Proposal) object;
        if ((this.proposalid == null && other.proposalid != null) || (this.proposalid != null && !this.proposalid.equals(other.proposalid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Proposal[ proposalid=" + proposalid + " ]";
    }
    
}
