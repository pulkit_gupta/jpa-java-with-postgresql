/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "blogentry")
@NamedQueries({
    @NamedQuery(name = "Blogentry.findAll", query = "SELECT b FROM Blogentry b"),
    @NamedQuery(name = "Blogentry.findByBlogid", query = "SELECT b FROM Blogentry b WHERE b.blogid = :blogid"),
    @NamedQuery(name = "Blogentry.findByTitle", query = "SELECT b FROM Blogentry b WHERE b.title = :title"),
    @NamedQuery(name = "Blogentry.findByContent", query = "SELECT b FROM Blogentry b WHERE b.content = :content"),
    @NamedQuery(name = "Blogentry.findByCreated", query = "SELECT b FROM Blogentry b WHERE b.created = :created"),
    @NamedQuery(name = "Blogentry.findByStatus", query = "SELECT b FROM Blogentry b WHERE b.status = :status")})
public class Blogentry implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "blogid")
    private Integer blogid;
    @Size(max = 100)
    @Column(name = "title")
    private String title;
    @Size(max = 2147483647)
    @Column(name = "content")
    private String content;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @ManyToOne(optional = false)
    private Parkuser userid;

    public Blogentry() {
    }

    public Blogentry(Integer blogid) {
        this.blogid = blogid;
    }

    public Blogentry(Integer blogid, Date created, String status) {
        this.blogid = blogid;
        this.created = created;
        this.status = status;
    }

    public Integer getBlogid() {
        return blogid;
    }

    public void setBlogid(Integer blogid) {
        this.blogid = blogid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkuser getUserid() {
        return userid;
    }

    public void setUserid(Parkuser userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (blogid != null ? blogid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Blogentry)) {
            return false;
        }
        Blogentry other = (Blogentry) object;
        if ((this.blogid == null && other.blogid != null) || (this.blogid != null && !this.blogid.equals(other.blogid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Blogentry[ blogid=" + blogid + " ]";
    }
    
}
