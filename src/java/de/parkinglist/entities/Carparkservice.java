/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkservice")
@NamedQueries({
    @NamedQuery(name = "Carparkservice.findAll", query = "SELECT c FROM Carparkservice c"),
    @NamedQuery(name = "Carparkservice.findByCpserviceid", query = "SELECT c FROM Carparkservice c WHERE c.cpserviceid = :cpserviceid"),
    @NamedQuery(name = "Carparkservice.findByTitle", query = "SELECT c FROM Carparkservice c WHERE c.title = :title"),
    @NamedQuery(name = "Carparkservice.findByDescription", query = "SELECT c FROM Carparkservice c WHERE c.description = :description"),
    @NamedQuery(name = "Carparkservice.findByStatus", query = "SELECT c FROM Carparkservice c WHERE c.status = :status")})
public class Carparkservice implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cpserviceid")
    private Integer cpserviceid;
    @Size(max = 63)
    @Column(name = "title")
    private String title;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Carparkservice() {
    }

    public Carparkservice(Integer cpserviceid) {
        this.cpserviceid = cpserviceid;
    }

    public Carparkservice(Integer cpserviceid, String status) {
        this.cpserviceid = cpserviceid;
        this.status = status;
    }

    public Integer getCpserviceid() {
        return cpserviceid;
    }

    public void setCpserviceid(Integer cpserviceid) {
        this.cpserviceid = cpserviceid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cpserviceid != null ? cpserviceid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkservice)) {
            return false;
        }
        Carparkservice other = (Carparkservice) object;
        if ((this.cpserviceid == null && other.cpserviceid != null) || (this.cpserviceid != null && !this.cpserviceid.equals(other.cpserviceid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkservice[ cpserviceid=" + cpserviceid + " ]";
    }
    
}
