/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "userdata")
@NamedQueries({
    @NamedQuery(name = "Userdata.findAll", query = "SELECT u FROM Userdata u"),
    @NamedQuery(name = "Userdata.findByUserdataid", query = "SELECT u FROM Userdata u WHERE u.userdataid = :userdataid"),
    @NamedQuery(name = "Userdata.findByStreet", query = "SELECT u FROM Userdata u WHERE u.street = :street"),
    @NamedQuery(name = "Userdata.findByZipcode", query = "SELECT u FROM Userdata u WHERE u.zipcode = :zipcode"),
    @NamedQuery(name = "Userdata.findByCity", query = "SELECT u FROM Userdata u WHERE u.city = :city"),
    @NamedQuery(name = "Userdata.findByCountry", query = "SELECT u FROM Userdata u WHERE u.country = :country"),
    @NamedQuery(name = "Userdata.findByPhone", query = "SELECT u FROM Userdata u WHERE u.phone = :phone"),
    @NamedQuery(name = "Userdata.findByMobile", query = "SELECT u FROM Userdata u WHERE u.mobile = :mobile"),
    @NamedQuery(name = "Userdata.findByFax", query = "SELECT u FROM Userdata u WHERE u.fax = :fax"),
    @NamedQuery(name = "Userdata.findByBirth", query = "SELECT u FROM Userdata u WHERE u.birth = :birth"),
    @NamedQuery(name = "Userdata.findByWorkarea", query = "SELECT u FROM Userdata u WHERE u.workarea = :workarea"),
    @NamedQuery(name = "Userdata.findByCompanyrole", query = "SELECT u FROM Userdata u WHERE u.companyrole = :companyrole"),
    @NamedQuery(name = "Userdata.findByCompany", query = "SELECT u FROM Userdata u WHERE u.company = :company"),
    @NamedQuery(name = "Userdata.findByBankaccountholder", query = "SELECT u FROM Userdata u WHERE u.bankaccountholder = :bankaccountholder"),
    @NamedQuery(name = "Userdata.findByBank", query = "SELECT u FROM Userdata u WHERE u.bank = :bank"),
    @NamedQuery(name = "Userdata.findByBankaccount", query = "SELECT u FROM Userdata u WHERE u.bankaccount = :bankaccount"),
    @NamedQuery(name = "Userdata.findByBankcode", query = "SELECT u FROM Userdata u WHERE u.bankcode = :bankcode"),
    @NamedQuery(name = "Userdata.findByIban", query = "SELECT u FROM Userdata u WHERE u.iban = :iban"),
    @NamedQuery(name = "Userdata.findByBic", query = "SELECT u FROM Userdata u WHERE u.bic = :bic"),
    @NamedQuery(name = "Userdata.findByPaypalemail", query = "SELECT u FROM Userdata u WHERE u.paypalemail = :paypalemail"),
    @NamedQuery(name = "Userdata.findByVat", query = "SELECT u FROM Userdata u WHERE u.vat = :vat")})
public class Userdata implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userdataid")
    private Integer userdataid;
    @Size(max = 127)
    @Column(name = "street")
    private String street;
    @Size(max = 31)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 63)
    @Column(name = "city")
    private String city;
    @Size(max = 2)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 63)
    @Column(name = "phone")
    private String phone;
    @Size(max = 31)
    @Column(name = "mobile")
    private String mobile;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 63)
    @Column(name = "fax")
    private String fax;
    @Column(name = "birth")
    @Temporal(TemporalType.DATE)
    private Date birth;
    @Size(max = 63)
    @Column(name = "workarea")
    private String workarea;
    @Size(max = 63)
    @Column(name = "companyrole")
    private String companyrole;
    @Size(max = 127)
    @Column(name = "company")
    private String company;
    @Size(max = 127)
    @Column(name = "bankaccountholder")
    private String bankaccountholder;
    @Size(max = 63)
    @Column(name = "bank")
    private String bank;
    @Column(name = "bankaccount")
    private Integer bankaccount;
    @Column(name = "bankcode")
    private Integer bankcode;
    @Size(max = 34)
    @Column(name = "iban")
    private String iban;
    @Size(max = 15)
    @Column(name = "bic")
    private String bic;
    @Size(max = 255)
    @Column(name = "paypalemail")
    private String paypalemail;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "vat")
    private String vat;
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @ManyToOne(optional = false)
    private Parkuser userid;

    public Userdata() {
    }

    public Userdata(Integer userdataid) {
        this.userdataid = userdataid;
    }

    public Userdata(Integer userdataid, String vat) {
        this.userdataid = userdataid;
        this.vat = vat;
    }

    public Integer getUserdataid() {
        return userdataid;
    }

    public void setUserdataid(Integer userdataid) {
        this.userdataid = userdataid;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getWorkarea() {
        return workarea;
    }

    public void setWorkarea(String workarea) {
        this.workarea = workarea;
    }

    public String getCompanyrole() {
        return companyrole;
    }

    public void setCompanyrole(String companyrole) {
        this.companyrole = companyrole;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBankaccountholder() {
        return bankaccountholder;
    }

    public void setBankaccountholder(String bankaccountholder) {
        this.bankaccountholder = bankaccountholder;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Integer getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(Integer bankaccount) {
        this.bankaccount = bankaccount;
    }

    public Integer getBankcode() {
        return bankcode;
    }

    public void setBankcode(Integer bankcode) {
        this.bankcode = bankcode;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getPaypalemail() {
        return paypalemail;
    }

    public void setPaypalemail(String paypalemail) {
        this.paypalemail = paypalemail;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public Parkuser getUserid() {
        return userid;
    }

    public void setUserid(Parkuser userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userdataid != null ? userdataid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userdata)) {
            return false;
        }
        Userdata other = (Userdata) object;
        if ((this.userdataid == null && other.userdataid != null) || (this.userdataid != null && !this.userdataid.equals(other.userdataid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Userdata[ userdataid=" + userdataid + " ]";
    }
    
}
