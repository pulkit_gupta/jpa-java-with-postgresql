/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "carparkbooking")
@NamedQueries({
    @NamedQuery(name = "Carparkbooking.findAll", query = "SELECT c FROM Carparkbooking c"),
    @NamedQuery(name = "Carparkbooking.findByBookingid", query = "SELECT c FROM Carparkbooking c WHERE c.bookingid = :bookingid"),
    @NamedQuery(name = "Carparkbooking.findByBookingDate", query = "SELECT c FROM Carparkbooking c WHERE c.bookingDate = :bookingDate"),
    @NamedQuery(name = "Carparkbooking.findByNrofspaces", query = "SELECT c FROM Carparkbooking c WHERE c.nrofspaces = :nrofspaces"),
    @NamedQuery(name = "Carparkbooking.findByPrice", query = "SELECT c FROM Carparkbooking c WHERE c.price = :price"),
    @NamedQuery(name = "Carparkbooking.findByAnrede", query = "SELECT c FROM Carparkbooking c WHERE c.anrede = :anrede"),
    @NamedQuery(name = "Carparkbooking.findByLastname", query = "SELECT c FROM Carparkbooking c WHERE c.lastname = :lastname"),
    @NamedQuery(name = "Carparkbooking.findByForename", query = "SELECT c FROM Carparkbooking c WHERE c.forename = :forename"),
    @NamedQuery(name = "Carparkbooking.findByPhone", query = "SELECT c FROM Carparkbooking c WHERE c.phone = :phone"),
    @NamedQuery(name = "Carparkbooking.findByEmail", query = "SELECT c FROM Carparkbooking c WHERE c.email = :email"),
    @NamedQuery(name = "Carparkbooking.findByStreet", query = "SELECT c FROM Carparkbooking c WHERE c.street = :street"),
    @NamedQuery(name = "Carparkbooking.findByZipcode", query = "SELECT c FROM Carparkbooking c WHERE c.zipcode = :zipcode"),
    @NamedQuery(name = "Carparkbooking.findByCity", query = "SELECT c FROM Carparkbooking c WHERE c.city = :city"),
    @NamedQuery(name = "Carparkbooking.findByCountry", query = "SELECT c FROM Carparkbooking c WHERE c.country = :country"),
    @NamedQuery(name = "Carparkbooking.findByCompany", query = "SELECT c FROM Carparkbooking c WHERE c.company = :company"),
    @NamedQuery(name = "Carparkbooking.findByBankaccountholder", query = "SELECT c FROM Carparkbooking c WHERE c.bankaccountholder = :bankaccountholder"),
    @NamedQuery(name = "Carparkbooking.findByBank", query = "SELECT c FROM Carparkbooking c WHERE c.bank = :bank"),
    @NamedQuery(name = "Carparkbooking.findByBankaccount", query = "SELECT c FROM Carparkbooking c WHERE c.bankaccount = :bankaccount"),
    @NamedQuery(name = "Carparkbooking.findByBankcode", query = "SELECT c FROM Carparkbooking c WHERE c.bankcode = :bankcode"),
    @NamedQuery(name = "Carparkbooking.findByIban", query = "SELECT c FROM Carparkbooking c WHERE c.iban = :iban"),
    @NamedQuery(name = "Carparkbooking.findByBic", query = "SELECT c FROM Carparkbooking c WHERE c.bic = :bic"),
    @NamedQuery(name = "Carparkbooking.findByUstIdnr", query = "SELECT c FROM Carparkbooking c WHERE c.ustIdnr = :ustIdnr"),
    @NamedQuery(name = "Carparkbooking.findByVehicleLicence", query = "SELECT c FROM Carparkbooking c WHERE c.vehicleLicence = :vehicleLicence"),
    @NamedQuery(name = "Carparkbooking.findByContractNbr", query = "SELECT c FROM Carparkbooking c WHERE c.contractNbr = :contractNbr"),
    @NamedQuery(name = "Carparkbooking.findByContractTrack", query = "SELECT c FROM Carparkbooking c WHERE c.contractTrack = :contractTrack"),
    @NamedQuery(name = "Carparkbooking.findByNote", query = "SELECT c FROM Carparkbooking c WHERE c.note = :note"),
    @NamedQuery(name = "Carparkbooking.findByStatus", query = "SELECT c FROM Carparkbooking c WHERE c.status = :status")})
public class Carparkbooking implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bookingid")
    private Integer bookingid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "booking_date")
    @Temporal(TemporalType.DATE)
    private Date bookingDate;
    @Column(name = "nrofspaces")
    private Integer nrofspaces;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Size(max = 15)
    @Column(name = "anrede")
    private String anrede;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 90)
    @Column(name = "lastname")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 90)
    @Column(name = "forename")
    private String forename;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 30)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 127)
    @Column(name = "street")
    private String street;
    @Size(max = 15)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 30)
    @Column(name = "city")
    private String city;
    @Size(max = 2)
    @Column(name = "country")
    private String country;
    @Size(max = 127)
    @Column(name = "company")
    private String company;
    @Size(max = 127)
    @Column(name = "bankaccountholder")
    private String bankaccountholder;
    @Size(max = 63)
    @Column(name = "bank")
    private String bank;
    @Column(name = "bankaccount")
    private Integer bankaccount;
    @Column(name = "bankcode")
    private Integer bankcode;
    @Size(max = 34)
    @Column(name = "iban")
    private String iban;
    @Size(max = 15)
    @Column(name = "bic")
    private String bic;
    @Size(max = 15)
    @Column(name = "ust_idnr")
    private String ustIdnr;
    @Size(max = 100)
    @Column(name = "vehicle_licence")
    private String vehicleLicence;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "contract_nbr")
    private String contractNbr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contract_track")
    private int contractTrack;
    @Size(max = 2147483647)
    @Column(name = "note")
    private String note;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carparkbooking")
    private List<BookedRates> bookedRatesList;

    public Carparkbooking() {
    }

    public Carparkbooking(Integer bookingid) {
        this.bookingid = bookingid;
    }

    public Carparkbooking(Integer bookingid, Date bookingDate, String lastname, String forename, String email, String contractNbr, int contractTrack, String status) {
        this.bookingid = bookingid;
        this.bookingDate = bookingDate;
        this.lastname = lastname;
        this.forename = forename;
        this.email = email;
        this.contractNbr = contractNbr;
        this.contractTrack = contractTrack;
        this.status = status;
    }

    public Integer getBookingid() {
        return bookingid;
    }

    public void setBookingid(Integer bookingid) {
        this.bookingid = bookingid;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Integer getNrofspaces() {
        return nrofspaces;
    }

    public void setNrofspaces(Integer nrofspaces) {
        this.nrofspaces = nrofspaces;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBankaccountholder() {
        return bankaccountholder;
    }

    public void setBankaccountholder(String bankaccountholder) {
        this.bankaccountholder = bankaccountholder;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Integer getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(Integer bankaccount) {
        this.bankaccount = bankaccount;
    }

    public Integer getBankcode() {
        return bankcode;
    }

    public void setBankcode(Integer bankcode) {
        this.bankcode = bankcode;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getUstIdnr() {
        return ustIdnr;
    }

    public void setUstIdnr(String ustIdnr) {
        this.ustIdnr = ustIdnr;
    }

    public String getVehicleLicence() {
        return vehicleLicence;
    }

    public void setVehicleLicence(String vehicleLicence) {
        this.vehicleLicence = vehicleLicence;
    }

    public String getContractNbr() {
        return contractNbr;
    }

    public void setContractNbr(String contractNbr) {
        this.contractNbr = contractNbr;
    }

    public int getContractTrack() {
        return contractTrack;
    }

    public void setContractTrack(int contractTrack) {
        this.contractTrack = contractTrack;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    public List<BookedRates> getBookedRatesList() {
        return bookedRatesList;
    }

    public void setBookedRatesList(List<BookedRates> bookedRatesList) {
        this.bookedRatesList = bookedRatesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookingid != null ? bookingid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carparkbooking)) {
            return false;
        }
        Carparkbooking other = (Carparkbooking) object;
        if ((this.bookingid == null && other.bookingid != null) || (this.bookingid != null && !this.bookingid.equals(other.bookingid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Carparkbooking[ bookingid=" + bookingid + " ]";
    }
    
}
