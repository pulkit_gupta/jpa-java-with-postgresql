/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "areacontact")
@NamedQueries({
    @NamedQuery(name = "Areacontact.findAll", query = "SELECT a FROM Areacontact a"),
    @NamedQuery(name = "Areacontact.findByAreacontactid", query = "SELECT a FROM Areacontact a WHERE a.areacontactid = :areacontactid"),
    @NamedQuery(name = "Areacontact.findByName", query = "SELECT a FROM Areacontact a WHERE a.name = :name"),
    @NamedQuery(name = "Areacontact.findByStreet", query = "SELECT a FROM Areacontact a WHERE a.street = :street"),
    @NamedQuery(name = "Areacontact.findByZipcode", query = "SELECT a FROM Areacontact a WHERE a.zipcode = :zipcode"),
    @NamedQuery(name = "Areacontact.findByCity", query = "SELECT a FROM Areacontact a WHERE a.city = :city"),
    @NamedQuery(name = "Areacontact.findByCountry", query = "SELECT a FROM Areacontact a WHERE a.country = :country"),
    @NamedQuery(name = "Areacontact.findByEmail", query = "SELECT a FROM Areacontact a WHERE a.email = :email"),
    @NamedQuery(name = "Areacontact.findByPhone", query = "SELECT a FROM Areacontact a WHERE a.phone = :phone"),
    @NamedQuery(name = "Areacontact.findByMobile", query = "SELECT a FROM Areacontact a WHERE a.mobile = :mobile"),
    @NamedQuery(name = "Areacontact.findByFax", query = "SELECT a FROM Areacontact a WHERE a.fax = :fax"),
    @NamedQuery(name = "Areacontact.findByStatus", query = "SELECT a FROM Areacontact a WHERE a.status = :status"),
    @NamedQuery(name = "Areacontact.findByTelefonclick", query = "SELECT a FROM Areacontact a WHERE a.telefonclick = :telefonclick")})
public class Areacontact implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "areacontactid")
    private Integer areacontactid;
    @Size(max = 127)
    @Column(name = "name")
    private String name;
    @Size(max = 127)
    @Column(name = "street")
    private String street;
    @Size(max = 31)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 63)
    @Column(name = "city")
    private String city;
    @Size(max = 3)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 127)
    @Column(name = "email")
    private String email;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "phone")
    private String phone;
    @Size(max = 31)
    @Column(name = "mobile")
    private String mobile;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 63)
    @Column(name = "fax")
    private String fax;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telefonclick")
    private int telefonclick;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Areacontact() {
    }

    public Areacontact(Integer areacontactid) {
        this.areacontactid = areacontactid;
    }

    public Areacontact(Integer areacontactid, String status, int telefonclick) {
        this.areacontactid = areacontactid;
        this.status = status;
        this.telefonclick = telefonclick;
    }

    public Integer getAreacontactid() {
        return areacontactid;
    }

    public void setAreacontactid(Integer areacontactid) {
        this.areacontactid = areacontactid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTelefonclick() {
        return telefonclick;
    }

    public void setTelefonclick(int telefonclick) {
        this.telefonclick = telefonclick;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (areacontactid != null ? areacontactid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Areacontact)) {
            return false;
        }
        Areacontact other = (Areacontact) object;
        if ((this.areacontactid == null && other.areacontactid != null) || (this.areacontactid != null && !this.areacontactid.equals(other.areacontactid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Areacontact[ areacontactid=" + areacontactid + " ]";
    }
    
}
