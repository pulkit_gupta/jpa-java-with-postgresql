/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "areacontact1")
@NamedQueries({
    @NamedQuery(name = "Areacontact1.findAll", query = "SELECT a FROM Areacontact1 a"),
    @NamedQuery(name = "Areacontact1.findByAreacontact1id", query = "SELECT a FROM Areacontact1 a WHERE a.areacontact1id = :areacontact1id"),
    @NamedQuery(name = "Areacontact1.findByName", query = "SELECT a FROM Areacontact1 a WHERE a.name = :name"),
    @NamedQuery(name = "Areacontact1.findByStreet", query = "SELECT a FROM Areacontact1 a WHERE a.street = :street"),
    @NamedQuery(name = "Areacontact1.findByZipcode", query = "SELECT a FROM Areacontact1 a WHERE a.zipcode = :zipcode"),
    @NamedQuery(name = "Areacontact1.findByCity", query = "SELECT a FROM Areacontact1 a WHERE a.city = :city"),
    @NamedQuery(name = "Areacontact1.findByCountry", query = "SELECT a FROM Areacontact1 a WHERE a.country = :country"),
    @NamedQuery(name = "Areacontact1.findByEmail", query = "SELECT a FROM Areacontact1 a WHERE a.email = :email"),
    @NamedQuery(name = "Areacontact1.findByPhone", query = "SELECT a FROM Areacontact1 a WHERE a.phone = :phone"),
    @NamedQuery(name = "Areacontact1.findByMobile", query = "SELECT a FROM Areacontact1 a WHERE a.mobile = :mobile"),
    @NamedQuery(name = "Areacontact1.findByFax", query = "SELECT a FROM Areacontact1 a WHERE a.fax = :fax"),
    @NamedQuery(name = "Areacontact1.findByStatus", query = "SELECT a FROM Areacontact1 a WHERE a.status = :status"),
    @NamedQuery(name = "Areacontact1.findByImpressum", query = "SELECT a FROM Areacontact1 a WHERE a.impressum = :impressum"),
    @NamedQuery(name = "Areacontact1.findByTelefonclick", query = "SELECT a FROM Areacontact1 a WHERE a.telefonclick = :telefonclick")})
public class Areacontact1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "areacontact1id")
    private Integer areacontact1id;
    @Size(max = 127)
    @Column(name = "name")
    private String name;
    @Size(max = 127)
    @Column(name = "street")
    private String street;
    @Size(max = 31)
    @Column(name = "zipcode")
    private String zipcode;
    @Size(max = 63)
    @Column(name = "city")
    private String city;
    @Size(max = 3)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 127)
    @Column(name = "email")
    private String email;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "phone")
    private String phone;
    @Size(max = 31)
    @Column(name = "mobile")
    private String mobile;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 63)
    @Column(name = "fax")
    private String fax;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "status")
    private String status;
    @Size(max = 2147483647)
    @Column(name = "impressum")
    private String impressum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telefonclick")
    private int telefonclick;
    @JoinColumn(name = "parkhausid", referencedColumnName = "parkhausid")
    @ManyToOne(optional = false)
    private Parkhaus parkhausid;

    public Areacontact1() {
    }

    public Areacontact1(Integer areacontact1id) {
        this.areacontact1id = areacontact1id;
    }

    public Areacontact1(Integer areacontact1id, String status, int telefonclick) {
        this.areacontact1id = areacontact1id;
        this.status = status;
        this.telefonclick = telefonclick;
    }

    public Integer getAreacontact1id() {
        return areacontact1id;
    }

    public void setAreacontact1id(Integer areacontact1id) {
        this.areacontact1id = areacontact1id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImpressum() {
        return impressum;
    }

    public void setImpressum(String impressum) {
        this.impressum = impressum;
    }

    public int getTelefonclick() {
        return telefonclick;
    }

    public void setTelefonclick(int telefonclick) {
        this.telefonclick = telefonclick;
    }

    public Parkhaus getParkhausid() {
        return parkhausid;
    }

    public void setParkhausid(Parkhaus parkhausid) {
        this.parkhausid = parkhausid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (areacontact1id != null ? areacontact1id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Areacontact1)) {
            return false;
        }
        Areacontact1 other = (Areacontact1) object;
        if ((this.areacontact1id == null && other.areacontact1id != null) || (this.areacontact1id != null && !this.areacontact1id.equals(other.areacontact1id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Areacontact1[ areacontact1id=" + areacontact1id + " ]";
    }
    
}
