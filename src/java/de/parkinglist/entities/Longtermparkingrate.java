/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pulkit
 */
@Entity
@Table(name = "longtermparkingrate")
@NamedQueries({
    @NamedQuery(name = "Longtermparkingrate.findAll", query = "SELECT l FROM Longtermparkingrate l"),
    @NamedQuery(name = "Longtermparkingrate.findByRateid", query = "SELECT l FROM Longtermparkingrate l WHERE l.rateid = :rateid"),
    @NamedQuery(name = "Longtermparkingrate.findByRateName", query = "SELECT l FROM Longtermparkingrate l WHERE l.rateName = :rateName"),
    @NamedQuery(name = "Longtermparkingrate.findByWebName", query = "SELECT l FROM Longtermparkingrate l WHERE l.webName = :webName"),
    @NamedQuery(name = "Longtermparkingrate.findByBookingModel", query = "SELECT l FROM Longtermparkingrate l WHERE l.bookingModel = :bookingModel"),
    @NamedQuery(name = "Longtermparkingrate.findByDescription", query = "SELECT l FROM Longtermparkingrate l WHERE l.description = :description"),
    @NamedQuery(name = "Longtermparkingrate.findByMinTime", query = "SELECT l FROM Longtermparkingrate l WHERE l.minTime = :minTime"),
    @NamedQuery(name = "Longtermparkingrate.findByPrice", query = "SELECT l FROM Longtermparkingrate l WHERE l.price = :price"),
    @NamedQuery(name = "Longtermparkingrate.findByAreatype", query = "SELECT l FROM Longtermparkingrate l WHERE l.areatype = :areatype"),
    @NamedQuery(name = "Longtermparkingrate.findBySettingDate", query = "SELECT l FROM Longtermparkingrate l WHERE l.settingDate = :settingDate"),
    @NamedQuery(name = "Longtermparkingrate.findByNote", query = "SELECT l FROM Longtermparkingrate l WHERE l.note = :note"),
    @NamedQuery(name = "Longtermparkingrate.findByStatus", query = "SELECT l FROM Longtermparkingrate l WHERE l.status = :status")})
public class Longtermparkingrate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rateid")
    private Integer rateid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "rate_name")
    private String rateName;
    @Size(max = 30)
    @Column(name = "web_name")
    private String webName;
    @Size(max = 20)
    @Column(name = "booking_model")
    private String bookingModel;
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "min_time")
    private int minTime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Size(max = 30)
    @Column(name = "areatype")
    private String areatype;
    @Basic(optional = false)
    @NotNull
    @Column(name = "setting_date")
    @Temporal(TemporalType.DATE)
    private Date settingDate;
    @Size(max = 2147483647)
    @Column(name = "note")
    private String note;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "longtermparkingrate")
    private List<BookedRates> bookedRatesList;
    @JoinColumn(name = "parkareaid", referencedColumnName = "parkareaid")
    @ManyToOne(optional = false)
    private Parkarea parkareaid;

    public Longtermparkingrate() {
    }

    public Longtermparkingrate(Integer rateid) {
        this.rateid = rateid;
    }

    public Longtermparkingrate(Integer rateid, String rateName, int minTime, Date settingDate, String status) {
        this.rateid = rateid;
        this.rateName = rateName;
        this.minTime = minTime;
        this.settingDate = settingDate;
        this.status = status;
    }

    public Integer getRateid() {
        return rateid;
    }

    public void setRateid(Integer rateid) {
        this.rateid = rateid;
    }

    public String getRateName() {
        return rateName;
    }

    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }

    public String getBookingModel() {
        return bookingModel;
    }

    public void setBookingModel(String bookingModel) {
        this.bookingModel = bookingModel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAreatype() {
        return areatype;
    }

    public void setAreatype(String areatype) {
        this.areatype = areatype;
    }

    public Date getSettingDate() {
        return settingDate;
    }

    public void setSettingDate(Date settingDate) {
        this.settingDate = settingDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BookedRates> getBookedRatesList() {
        return bookedRatesList;
    }

    public void setBookedRatesList(List<BookedRates> bookedRatesList) {
        this.bookedRatesList = bookedRatesList;
    }

    public Parkarea getParkareaid() {
        return parkareaid;
    }

    public void setParkareaid(Parkarea parkareaid) {
        this.parkareaid = parkareaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateid != null ? rateid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Longtermparkingrate)) {
            return false;
        }
        Longtermparkingrate other = (Longtermparkingrate) object;
        if ((this.rateid == null && other.rateid != null) || (this.rateid != null && !this.rateid.equals(other.rateid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.parkinglist.entities.Longtermparkingrate[ rateid=" + rateid + " ]";
    }
    
}
