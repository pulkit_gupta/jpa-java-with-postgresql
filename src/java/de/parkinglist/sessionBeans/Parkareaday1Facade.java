/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.sessionBeans;

import de.parkinglist.entities.Parkareaday1;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author pulkit
 */
@Stateless
public class Parkareaday1Facade extends AbstractFacade<Parkareaday1> {
    @PersistenceContext(unitName = "test1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Parkareaday1Facade() {
        super(Parkareaday1.class);
    }
    
}
