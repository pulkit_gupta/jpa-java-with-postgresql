/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.sessionBeans;

import de.parkinglist.controllers.AirportPriceManager;
import de.parkinglist.entities.Airportpricing;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author pulkit
 */
@Stateless
public class AirportpricingFacade extends AbstractFacade<Airportpricing> {
    protected static final Logger logger = LogManager.getLogger(AirportpricingFacade.class.getName());

    
    @PersistenceContext(unitName = "test1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AirportpricingFacade() {
        super(Airportpricing.class);
    }
    
    
    public List<Airportpricing> getPricingList(){
        return null;
    }

    
    /**
     * 
     * Get list of all the pricing for the list of months
     * 
     * @param parkAreaId
     * @param months
     * @return 
     */
    public List<Airportpricing> getListByParkAreaId(int parkAreaId, List months) {
        
        String SQLquery = "SELECT ap FROM Airportpricing ap where ap.parkareaid.parkareaid = :parkAreaId AND (";
        
        int i =0;
        for (Iterator iterator = months.iterator(); iterator.hasNext();) {
            Integer next = (Integer) iterator.next();
                     
            
            SQLquery +=" ap.month = :month"+String.valueOf(i);                        
            i++;
            if(iterator.hasNext()){
                SQLquery += " OR";
            }
         
        }
             
        SQLquery += ")";
        
        logger.info("QUERY IS ->   "+SQLquery);
        
        Query q = getEntityManager().createQuery(SQLquery);
        q.setParameter("parkAreaId", parkAreaId);
        
        for (int j = 0; j < i; j++) {
            q.setParameter("month"+j, months.get(j));
        }
        
        return q.getResultList();                
    }
    
    
    
    
//    
//    /**
//     * 
//     * NOT USED 
//     * 
//     * @param parkAreaId
//     * @param month
//     * @return 
//     */
//    public List<Airportpricing> updateListByParkAreaId(int parkAreaId, int month) {
//        
//        String SQLquery = "SELECT ap FROM Airportpricing ap where ap.parkareaid.parkareaid = :parkAreaId AND (ap.month = :month)";
//        
//        
//        
//        logger.info("QUERY IS ->   "+SQLquery);
//        
//        Query q = getEntityManager().createQuery(SQLquery);
//        
//        q.setParameter("parkAreaId", parkAreaId);
//        q.setParameter("month", month);
//        
//        Airportpricing arpPricing = (Airportpricing) q.getSingleResult();
//                
//        
//        return q.getResultList();                
//    }
//    
   
}
