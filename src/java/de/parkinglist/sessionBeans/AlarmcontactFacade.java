/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.parkinglist.sessionBeans;

import de.parkinglist.entities.Alarmcontact;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author pulkit
 */
@Stateless
public class AlarmcontactFacade extends AbstractFacade<Alarmcontact> {
    @PersistenceContext(unitName = "test1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AlarmcontactFacade() {
        super(Alarmcontact.class);
    }
    
}
